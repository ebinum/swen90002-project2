package swen90002_21.proj2.models;

import java.sql.Timestamp;
import java.util.Date;


/*
 * This is Chat class
 */
public class ChatMessage {
    private String message;
	private String user;
	private String camelCaseObject;
	private Timestamp timestamp;    
    
    public ChatMessage(String message, String user) {
        this.message = message;
        this.setUser(user);
        this.timestamp = new Timestamp(new Date().getTime());
        camelCaseObject = "hello";
    }

	/**
     * @return the message
     */
    public synchronized String getMessage() {
        return message;
    }
    
    /**
     * @param message the message to set
     */
    public synchronized void setMessage(String message) {
        this.message = message;
    }
    
    public String toString() {
        return message; 
    }

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getCamelCaseObject() {
		return camelCaseObject;
	}

	public void setCamelCaseObject(String camelCaseObject) {
		this.camelCaseObject = camelCaseObject;
	}
}