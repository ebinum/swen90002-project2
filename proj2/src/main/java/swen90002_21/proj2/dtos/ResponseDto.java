package swen90002_21.proj2.dtos;

public class ResponseDto {

	private String message;
	private String status;

	public ResponseDto(String status, String message) {
		// TODO Auto-generated constructor stub
		this.setStatus(status);
		this.setMessage(message);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
