package swen90002_21.proj2.dtos;

public class LoginResponseDto extends ResponseDto {

	private String accessToken;
	private String email;

	public LoginResponseDto(String status, String message,String accessToken,String email) {
		super(status, message);
		this.setAccessToken(accessToken);
		this.setEmail(email);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

}
