package swen90002_21.proj2.dtos;

public class GroupsDto {
	
	private String groupName;
	private String owner;
	private String[] acceptedMembers;
	private String[] invitedMembers;
	
	public GroupsDto() {
	}
	public GroupsDto(String groupName, String owner, String[] acceptedMembers,
			String[] invitedMembers, String groupId) {
		this.groupName = groupName;
		this.owner = owner;
		this.acceptedMembers = acceptedMembers;
		this.invitedMembers = invitedMembers;
		this.groupId = groupId;
	}
	private String groupId;
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String[] getAcceptedMembers() {
		return acceptedMembers;
	}
	public void setAcceptedMembers(String[] acceptedMembers) {
		this.acceptedMembers = acceptedMembers;
	}
	public String[] getInvitedMembers() {
		return invitedMembers;
	}
	public void setInvitedMembers(String[] invitedMembers) {
		this.invitedMembers = invitedMembers;
	}
	
	 
}
