package swen90002_21.proj2.dtos;

public class GroupsResponseDto extends ResponseDto {

	private GroupsDto[] groups;

	public GroupsResponseDto(String status, String message,GroupsDto[] groups) {
		super(status, message);
		this.setGroups(groups);
	}

	public GroupsDto[] getGroups() {
		return groups;
	}

	public void setGroups(GroupsDto[] groups) {
		this.groups = groups;
	}

}
