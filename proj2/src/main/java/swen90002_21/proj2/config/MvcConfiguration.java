package swen90002_21.proj2.config;

import java.util.List;
import java.util.Properties;

import org.lightcouch.CouchDbClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;

import swen90002_21.proj2.handlers.AuthenticationFailureHandler;
import swen90002_21.proj2.handlers.AuthenticationSuccessHandler;
import swen90002_21.proj2.services.GroupService;
import swen90002_21.proj2.services.GroupServiceImpl;
import swen90002_21.proj2.services.HelperService;
import swen90002_21.proj2.services.Mailer;

@Configuration
@ComponentScan(basePackages="swen90002_21.proj2")
@EnableWebMvc
//@PropertySource("classpath:/com/myco/app.properties")
public class MvcConfiguration extends WebMvcConfigurerAdapter{

	public MvcConfiguration(){
		
	}
	
	@Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
	}
	

//	@Override
//    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
//		//add mapper that will convert messages to camel case
//		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
//		ObjectMapper objectMapper = new ObjectMapper();
//		objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES
//			    );
//		converter.setObjectMapper(objectMapper);
//        converters.add(converter);
//    }

	
//	@Bean
//	@Resource(name="mail")
//	public Properties mail;
//	
	@Bean
	 public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
	  return new PropertySourcesPlaceholderConfigurer();
	 }

	//Define beans
	@Bean
	public ViewResolver getViewResolver(){
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".html");
		return resolver;
	}
	
	@Bean//(initMethod = "syncDesignDocsWithDb", destroyMethod = "shutdown" )
	//@Scope(value = "singleton", proxyMode = ScopedProxyMode.TARGET_CLASS)
	public CouchDbClient dbClient(){
		return new CouchDbClient();
	}
	
	@Bean
	@Scope(value = "singleton", proxyMode = ScopedProxyMode.TARGET_CLASS)
	public Mailer mailer(){
		Properties props = new Properties();
//		props.put("mail.smtp.auth", "true");
//		props.put("mail.smtp.starttls.enable", "true");
//		props.put("mail.smtp.EnableSSL.enable","true");
//		props.put("mail.smtp.host", "smtp.gmail.com");
//		props.put("mail.smtp.port", "587");
		
		props.put("mail.smtp.host", "localhost");
		props.put("mail.smtp.port", "25");
		
		//System.out.print(mail);
		return Mailer.getInstance(props);
	}
	
	@Bean
	@Scope(value = "singleton", proxyMode = ScopedProxyMode.DEFAULT)
	public HelperService helper(){
		return new HelperService();
	}
	
	@Bean
	public AuthenticationFailureHandler authenticationFailureHandler(){
		return new AuthenticationFailureHandler();
	}
	
	@Bean
	public AuthenticationSuccessHandler authenticationSuccessHandler(){
		return new AuthenticationSuccessHandler();
	}
	
	@Bean 
	public GroupService groupService(){
		return new GroupServiceImpl();
	}
	
	
	
	
//	@Bean
//	@Scope(value = "prototype", proxyMode = ScopedProxyMode.INTERFACES)
//	public CouchDbClient userDoa(){
//		return DbClientFactory.getInstance();
//	}
	
	
	

}
