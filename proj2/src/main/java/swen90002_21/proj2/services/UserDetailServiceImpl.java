package swen90002_21.proj2.services;

import java.util.ArrayList;
import java.util.List;

import org.lightcouch.CouchDbClient;
import org.lightcouch.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.google.gson.JsonObject;

public class UserDetailServiceImpl implements UserDetailsService {

	@Autowired
	private CouchDbClient client;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		List<JsonObject> query;
		try {
			View results = client.view("user_view/user").key(username);

			query = results.query(JsonObject.class);
			if (query == null || query.size() == 0)
				throw new UsernameNotFoundException("user name not found");

		} catch (Exception e) {
			throw new UsernameNotFoundException("database error ");
		}
		return buildUserFromUserEntity(query.get(0));
	}

	private UserDetails buildUserFromUserEntity(JsonObject jsonObject) {
		String username = jsonObject.get("username").getAsString();
		String password = "";
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		authorities.set(0,new GrantedAuthorityImpl(jsonObject.get("role").getAsString()));

		User springUser = new User(username, password, authorities);
		return springUser;

	}

}
