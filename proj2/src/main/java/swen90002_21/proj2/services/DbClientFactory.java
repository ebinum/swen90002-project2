package swen90002_21.proj2.services;

import org.lightcouch.CouchDbClient;
import org.lightcouch.CouchDbProperties;

public class DbClientFactory {
    static CouchDbClient instance = null;
    static CouchDbProperties prop = new CouchDbProperties()
    .setDbName("proj2")
    .setCreateDbIfNotExist(true)
    .setHost("localhost")
    .setPort(5984)
    .setProtocol("http"); 
    
    
    
    private DbClientFactory() {
              
    }
    
    public static CouchDbClient getInstance() {
        if(instance == null) {
            instance = new CouchDbClient(prop); 
         }
         return instance;
    }

}
