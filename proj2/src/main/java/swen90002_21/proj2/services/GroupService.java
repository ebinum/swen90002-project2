package swen90002_21.proj2.services;

import java.util.List;

import com.google.gson.JsonObject;

public interface GroupService {

	public  List<JsonObject>  getGroupMessage(String groupName, String username, String owner);
	
	public List<JsonObject> getAllGroups(String username);

	public  String postChatToGroup(String groupName, String chatMessage,
			String username, String owner);

	public  void leaveGroup(String groupName, String username);

	public  String deleteGroup(String groupName, String username);

	public  String deleteFromGroup(String groupName, String username,
			String userToDelete);

	public  String inviteToGroup(String groupName, String username, String invitedUsername);	
	
	public  String inviteToAccList(String groupName, String username, String invitedUsername);
	

	public  String createGroup(String groupName, String username);
}
