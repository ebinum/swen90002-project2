package swen90002_21.proj2.services;

import java.util.ArrayList;
import java.util.List;

import org.lightcouch.CouchDbClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import swen90002_21.proj2.controller.BaseController;
import swen90002_21.proj2.models.ChatMessage;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Service
public class GroupServiceImpl implements GroupService {

	public GroupServiceImpl() {
	}

	JsonObject Jobject = new JsonObject();
	JsonObject addMember = new JsonObject();
	JsonObject responseObject = new JsonObject();
	JsonParser parser = new JsonParser();
	@Autowired
	private CouchDbClient dbClient;
	
//	@Override
//	@Autowired
//	public void setDbClient(CouchDbClient dbClient) {
//		this.dbClient = dbClient;
//		System.out.println("Auto wired fired");
//	}
	
	@Autowired
	Mailer mailer;
//	
	
	public List<JsonObject> getAllGroups(String username) {		
		
		System.out.println("Group was hit");
		//Case 1: To get all own groups
		List<JsonObject> docs = dbClient.view("groups/allgroups").key(username)
				.query(JsonObject.class);
		
		
		List<JsonObject> otherdocs = dbClient.view("groups/othergroups")
				.query(JsonObject.class);
		
		
		List<JsonObject> gNames = new ArrayList<JsonObject>();	
		int docscounter = 0, alldocsCounter = 0; 
		
		System.out.println("Docs size " + docs.size());
		//To add user groups to list
		if (!docs.isEmpty())
		{			
			for (int i=0; i<docs.size(); i++)
			{
				String id = docs.get(i).get("id").getAsString();				
				JsonObject son = dbClient.find(JsonObject.class, id);
				System.out.println("Returning son " + son);				
				
				String owner = son.get("owner").getAsString();
				
				if (owner.equals(username))
				{					
					//System.out.println("print 1 " + owner+ "\n"+ docs);
					docscounter++;					
				}
			}
			
			if (docscounter >=1) {
				//System.out.println("print 1 " + docs);
				gNames.addAll(docs);
			}
		}		
		
		
		//To add groups if user exits in any group
		if (!otherdocs.isEmpty()) {
			for (int i=0; i<otherdocs.size(); i++) {
				String id = otherdocs.get(i).get("id").getAsString();				
				JsonObject son = dbClient.find(JsonObject.class, id);
				
//				System.out.println(" ------------------------------------------- ");
				
				JsonArray key = son.getAsJsonObject("group")
						.getAsJsonArray("acceptedMembers");
				
//				System.out.println("Im printing key : " + key);				
				for (JsonElement s : key) {
					String user = s.getAsString();
					
					if (user.equals(username)) {
//						System.out.println("Actual Document : " +otherdocs.get(i));
						gNames.add(otherdocs.get(i));
					}
				}
			}
		}		
		//Returning total list
		return (gNames);
	}

	public String createGroup(String groupName, String username) {
		List<JsonObject> docs = dbClient.view("groups/allgroups").key(username)
				.query(JsonObject.class);
		
		int groupChk = 0;

		if (!docs.isEmpty()) {
			
			//To check if group exits
			for (int i=0; i<docs.size(); i++) {
				String id = docs.get(i).get("id").getAsString();
				JsonObject son = dbClient.find(JsonObject.class, id);				
				String groupname = son.get("group_name").getAsString();		
				
				System.out.println("group chekc was hit "+groupname+ " --  " + groupName +"  d0c size" + docs.size());
				
				if (groupname.equals(groupName))
				{
					groupChk++;
					break;
				}
			}
		}
//		else {
//			
//		}
		
			//To create group if not exits
			if (groupChk == 0) {			
				String json_text = "{\"group\": {\"invitedMembers\": [ ],\"acceptedMembers\":[ ] }}";
				JsonObject o = (JsonObject) parser.parse(json_text);
				o.addProperty("group_name", groupName);
				o.addProperty("owner", username);
				o.addProperty("type", "group");

				dbClient.save(o);
				System.out.println("Created");						
				
				return "created";
			}			
			else {
				return "not";
			}		
		}
	

	public String inviteToGroup(String groupName, String username,
			String invitedUsername) {
		List<JsonObject> docs = dbClient.view("groups/allgroups").key(username)
				.query(JsonObject.class);
		List<JsonObject> userDocs = dbClient.view("user/allusers").key(invitedUsername)
				.query(JsonObject.class);
		int counter = 0;

		if (!userDocs.isEmpty()) { 
		System.out.println("method was hit " + docs.size());
		if (!docs.isEmpty()) {
			for (int i = 0; i < docs.size(); i++) {
				
				String id = docs.get(i).get("id").getAsString();
				System.out.println("Loop started " + id );
				JsonObject son = dbClient.find(JsonObject.class, id);
				String rev = son.get("_rev").getAsString();
				String owner = son.get("owner").getAsString();
				String groupname = son.get("group_name").getAsString();
				
				System.out.println("GN: "+groupname + "Owner "+ owner);

				if (groupName.equals(groupname) && owner.equals(username)) {

					JsonArray invitedData = son.getAsJsonObject("group")
							.getAsJsonArray("invitedMembers");
					JsonArray acceptedData = son.getAsJsonObject("group")
							.getAsJsonArray("acceptedMembers");

					JsonObject obj = new JsonObject();
					obj.addProperty("newUser", invitedUsername);

					JsonElement test = obj.get("newUser");

					JsonArray finalArray = new JsonArray();
					invitedData.add(test);

					String json_text = "{\"group\": {\"invitedMembers\": "
							+ invitedData + ",\"acceptedMembers\":"
							+ acceptedData + "}}";

					JsonObject o = new JsonObject();

					o = (JsonObject) parser.parse(json_text);
					o.addProperty("_id", id);
					o.addProperty("_rev", rev);
					o.addProperty("owner", owner);
					o.addProperty("group_name", groupName);
					o.addProperty("type", "group");
					dbClient.update(o);
					/*
					 * To do email function
					 * http://localhost:8088/proj2/Groups/{groupname}/inviteToAccList/{invited user email}/?username={owner}
					 */
					
					mailer.SendMail(groupName, invitedUsername, username);
					
					counter++;
					
					System.out.println("Added user to invite list");
					break;
				} else {
					System.out.println("Wrong group name");
				}
				
			}			
		} 
		}
		
		System.out.println("counter value " + counter);
		//Sending bak to controller
		if (counter >= 1) {
			return "Added";
		}
		else {
			return "Wrong";
		}
	}
	
	//Invite to accept List
	public String inviteToAccList(String groupName, String username,
			String invitedUsername) {
		List<JsonObject> docs = dbClient.view("groups/allgroups").key(username)
				.query(JsonObject.class);
		List<JsonObject> userDocs = dbClient.view("user/allusers").key(invitedUsername)
				.query(JsonObject.class);
		int counter = 0;

		if (!userDocs.isEmpty()) { 
		System.out.println("method was hit " + docs.size());
		if (!docs.isEmpty()) {
			for (int i = 0; i < docs.size(); i++) {
				
				String id = docs.get(i).get("id").getAsString();
				System.out.println("Loop started " + id );
				JsonObject son = dbClient.find(JsonObject.class, id);
				String rev = son.get("_rev").getAsString();
				String owner = son.get("owner").getAsString();
				String groupname = son.get("group_name").getAsString();
				
				System.out.println("GN: "+groupname + "Owner "+ owner);

				if (groupName.equals(groupname) && owner.equals(username)) {

					JsonArray invitedData = son.getAsJsonObject("group")
							.getAsJsonArray("invitedMembers");
					JsonArray acceptedData = son.getAsJsonObject("group")
							.getAsJsonArray("acceptedMembers");

					JsonObject obj = new JsonObject();
					obj.addProperty("newUser", invitedUsername);

					JsonElement test = obj.get("newUser");
					//Added to Accepeted data list
					acceptedData.add(test);
					JsonArray finalInvitedArray = new JsonArray();
					
					//Removing from invited Data list
					for (JsonElement s : invitedData) {
						String invitee = s.getAsString();
						
						if (!invitee.equals(invitedUsername)) {
							finalInvitedArray.add(s);
						}
					}
					

					String json_text = "{\"group\": {\"invitedMembers\": "
							+ finalInvitedArray + ",\"acceptedMembers\":"
							+ acceptedData + "}}";

					JsonObject o = new JsonObject();

					o = (JsonObject) parser.parse(json_text);
					o.addProperty("_id", id);
					o.addProperty("_rev", rev);
					o.addProperty("owner", owner);
					o.addProperty("group_name", groupName);
					o.addProperty("type", "group");
					dbClient.update(o);
					counter++;
					
					System.out.println("Added user to invite list");
					break;
				} else {
					System.out.println("Wrong group name");
				}
				
			}			
		} 
		}
		
		System.out.println("counter value " + counter);
		//Sending bak to controller
		if (counter >= 1) {
			return "Added";
		}
		else {
			return "Wrong";
		}
	}

	
	public String deleteFromGroup(String groupName, String username,
			String userToDelete) {
		List<JsonObject> docs = dbClient.view("groups/allgroups").key(username)
				.query(JsonObject.class);
		
		System.out.println("Service was hit  " + docs.size());
		
		int counter = 0;

		if (!docs.isEmpty()) {
			for (int i=0; i<docs.size(); i++)
			{
				String id = docs.get(i).get("id").getAsString();
				
				System.out.println(id);
				
				JsonObject son = dbClient.find(JsonObject.class, id);
				String rev = son.get("_rev").getAsString();
				String owner = son.get("owner").getAsString();
				String groupname = son.get("group_name").getAsString();
				
				System.out.println(owner +" -- " + groupname + " Docs size : "+ docs.size());
				
				if (groupname.equals(groupName)) {
					JsonArray invitedData = son.getAsJsonObject("group")
							.getAsJsonArray("invitedMembers");
					JsonArray acceptedData = son.getAsJsonObject("group")
							.getAsJsonArray("acceptedMembers");

					JsonArray finalAcceptedData = new JsonArray();
					
					for (JsonElement s : acceptedData) {
										
						String t = s.getAsString();
						if (!t.equals(userToDelete))
						{
							finalAcceptedData.add(s);
						}				
					}
					
					String json_text = "{\"group\": {\"invitedMembers\": "
							+ invitedData + ",\"acceptedMembers\":"
							+ finalAcceptedData + "}}";
					
					JsonObject o = new JsonObject();

					o = (JsonObject) parser.parse(json_text);
					o.addProperty("_id", id);
					o.addProperty("_rev", rev);
					o.addProperty("owner", owner);
					o.addProperty("group_name", groupName);
					o.addProperty("type", "group");
					dbClient.update(o);
					counter++;

					break;
				}
				else
				{
					System.out.println("Wrong groupName");
				}
			}
		}
		else
		{
			System.out.println("No match");
		}
		
		return (counter >=1) ?  "Removed" : "Wrong";		
	}

	
	public String deleteGroup(String groupName, String username) {
		List<JsonObject> docs = dbClient.view("groups/allgroups").key(username)
				.query(JsonObject.class);
		
		List<JsonObject> msgDocs = dbClient.view("messages/groupMsgs").key(username)
				.query(JsonObject.class);

		int counter = 0;
		if (!docs.isEmpty()) {			
			//To delete the group
			for (int i=0; i<docs.size(); i++)
			{
				String id = docs.get(i).get("id").getAsString();
				JsonObject son = dbClient.find(JsonObject.class, id);
				String rev = son.get("_rev").getAsString();				
				String groupname = son.get("group_name").getAsString();
				String owner = son.get("owner").getAsString();
				
				if (groupname.equals(groupName) && owner.equals(username))
				{
					dbClient.remove(id, rev);
					counter++;
					break;
				}
			}			
		}
		
		//To delete the messages belongs to group
		if (!msgDocs.isEmpty()) {
			
			for (int i=0; i<msgDocs.size(); i++) {
				String id1 = msgDocs.get(i).get("id").getAsString();
				JsonObject son = dbClient.find(JsonObject.class, id1);
				String rev1 = son.get("_rev").getAsString();				
				String groupname1 = son.get("group_name").getAsString();
				String owner1 = son.get("owner").getAsString();
				
				if (groupname1.equals(groupName) && owner1.equals(username)) {
					dbClient.remove(id1, rev1);
					
				}
				
			}
		}
		
		if (counter >= 1) {
			return "Deleted";
		}
		else {
			return "Wrong";
		}
	}

	
	public void leaveGroup(String groupName, String username) {

	}
	
	public String postChatToGroup(String groupName, String chatMessage,
			String username, String owner) {
		
		List<JsonObject> groupDocs = dbClient.view("groups/allgroups").key(owner)
				.query(JsonObject.class);	
		
		int counter = 0;
		
		if (!groupDocs.isEmpty()) {
			
			for (int i=0; i<groupDocs.size(); i++) {
			String id = groupDocs.get(i).get("id").getAsString();
			JsonObject son = dbClient.find(JsonObject.class, id);
			String gName = son.get("group_name").getAsString();
			
			if (gName.equals(groupName)) {
				
				ChatMessage message = new ChatMessage(chatMessage, username);
				
				System.out.println("1");
				JsonObject obj = new JsonObject();
				
				obj.addProperty("group_name", groupName);
				obj.addProperty("message_text", message.toString());
				obj.addProperty("timestamp", message.getTimestamp().toString());
				obj.addProperty("type", "message");
				obj.addProperty("posted_by", message.getUser());
				obj.addProperty("owner", owner);
				
				System.out.println("2");
				dbClient.save(obj);
				System.out.println("3");
				System.out.println("Data has been saved");
				
				counter++;
				break;
				}
			}
			
			return (counter >=1) ?  "Saved" : "Wrong";
			
		}
		
		
		ChatMessage message = new ChatMessage(chatMessage, username);
		
		System.out.println("1");
		JsonObject obj = new JsonObject();
		
		obj.addProperty("group_name", groupName);
		obj.addProperty("message_txt", message.toString());
		obj.addProperty("timestamp", message.getTimestamp().toString());
		obj.addProperty("type", "message");
		obj.addProperty("posted_by", message.getUser());
		obj.addProperty("owner", owner);
		
		System.out.println("2");
		dbClient.save(obj);
		System.out.println("3");
		System.out.println("Data has been saved");
		
		return "Done";
	}
	
	public List<JsonObject> getGroupMessage(String groupName, String username, String owner) {
		
//		System.out.println("GroupName :" + groupName + " Requested user : " + username +"  Owner : " + owner);
		
		List<JsonObject> groupDocs = dbClient.view("groups/allgroups").key(owner)
				.query(JsonObject.class);		
		List<JsonObject> msgDocs = dbClient.view("messages/groupMsgs").key(owner)
				.query(JsonObject.class);	
//		List<JsonObject> msgDocs2 = dbClient.view("groupMsg/allGroupMessages").startKey(owner,groupName)
//			.query(JsonObject.class);
//		System.out.println("Message docs size  " + msgDocs.size());
		
//		System.out.println("Group MEssages  "+ msgDocs2);
		List<JsonObject> msgs = new ArrayList<JsonObject>();
		
//		System.out.println(groupDocs.size());
		
		if (!groupDocs.isEmpty()) {
			
			for (int i=0 ; i<msgDocs.size(); i++) {
				
				String id = msgDocs.get(i).get("id").getAsString();
				JsonObject son = dbClient.find(JsonObject.class, id);
				String grName = son.get("group_name").getAsString();
				
				
//				JsonArray acceptedData = son.getAsJsonObject("group")
//						.getAsJsonArray("acceptedMembers");
				
				if (grName.equals(groupName)) {
					JsonObject so = new JsonObject();						
					
					so.addProperty("group_name", son.get("group_name").getAsString());
					so.addProperty("message_text", son.get("message_text").getAsString());
					so.addProperty("timestamp", son.get("timestamp").getAsString());
					so.addProperty("posted_by", son.get("posted_by").getAsString());
					so.addProperty("owner", son.get("owner").getAsString());
					
					msgs.add(so);					
				}			
			}		
		}				
		return (msgs);		
	}	
}
