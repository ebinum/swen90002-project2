package swen90002_21.proj2.services;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class Mailer {
    
    private static Mailer instance = null;
    
    static Properties props = new Properties();
    

    public static Mailer getInstance(Properties property) {
       if(instance == null) {
          instance = new Mailer(property);
       }
       return instance;
    }    
    
    public Mailer(){}
    private Mailer(Properties property) {
    	props = property;
//        // TODO Auto-generated constructor stub    	
    }
    
    //Code to send an activation link to join chat group
    public void SendMail(String group, String invitee, String owner) {
    	Session session = Session.getInstance(props);  //,
//				  new javax.mail.Authenticator() {
//					protected PasswordAuthentication getPasswordAuthentication() {
//						return new PasswordAuthentication("swen90002ia@gmail.com", "iacourse");
//					}
//				  });
    	
    	try {
   		 
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("vyeluri@swen90002-21.cis.unimelb.edu.au"));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(""+ invitee +""));
			message.setSubject("SWEN90002 group chat invite");
			message.setText("Hi, \n"
					+ "Click the below link to join chat \n"
					+ "Note: Click the link after you login in application\n\n"
					+ "http://swen90002-21.cis.unimelb.edu.au:8080/proj2/Groups/"+ group +"/inviteToAccList/" + invitee +"/?username="+ owner +"\n\n"
							+ "Regards,\n"
							+ "Group - 5");
 
			Transport.send(message);
 
			System.out.println("Done");
 
		} catch (MessagingException e) {
			System.out.println("Failed");
			throw new RuntimeException(e);
		}
    }
    
    
    //Code to send Reminder about password
    
    public void SendMail(String username, String password) {
        // TODO Add logic for sending email
       	
    	System.out.println("Mail is working as expected");
    	
       	Session session = Session.getInstance(props);
       	
       	try {
      		 
   			Message message = new MimeMessage(session);
   			message.setFrom(new InternetAddress("vyeluri@swen90002-21.cis.unimelb.edu.au"));
   			message.setRecipients(Message.RecipientType.TO,
   				InternetAddress.parse(username));
   			message.setSubject("SWEN90002 group chat password recovery");
   			message.setText("Hi, \n"
   					+ "Use the below username and password to login to the chat application \n"
   					+ "username: "+ username +"\n"
   							+ "password: "+ password +"\n\n"
   									+ "Regards,"
   									+ "Group - 5");
    
   			Transport.send(message);
    
   			System.out.println("Done");
    
   		} catch (MessagingException e) {
   			System.out.println("Failed");
   			throw new RuntimeException(e);
   		}       	
       }
    
    public void Register(String msg, String email) {
        // TODO Add logic for sending email
       	
    	System.out.println("Mail is working as expected");
    	
       	Session session = Session.getInstance(props);
       	
       	try {
      		 
   			Message message = new MimeMessage(session);
   			message.setFrom(new InternetAddress("vyeluri@swen90002-21.cis.unimelb.edu.au"));
   			message.setRecipients(Message.RecipientType.TO,
   				InternetAddress.parse(email));
   			message.setSubject("SWEN90002 group chat Registration confirmation");
   			message.setText(msg);
    
   			Transport.send(message);
    
   			System.out.println("Done");
    
   		} catch (MessagingException e) {
   			System.out.println("Failed");
   			throw new RuntimeException(e);
   		}       	
       }
    
    public void Reset(String msg, String email) {
        // TODO Add logic for sending email
       	
    	System.out.println("Mail is working as expected");
    	
       	Session session = Session.getInstance(props);
       	
       	try {
      		 
   			Message message = new MimeMessage(session);
   			message.setFrom(new InternetAddress("vyeluri@swen90002-21.cis.unimelb.edu.au"));
   			message.setRecipients(Message.RecipientType.TO,
   				InternetAddress.parse(email));
   			message.setSubject(msg);
    
   			Transport.send(message);
    
   			System.out.println("Done");
    
   		} catch (MessagingException e) {
   			System.out.println("Failed");
   			throw new RuntimeException(e);
   		}       	
       }
    
    public void Confirm(String msg, String email) {
        // TODO Add logic for sending email  	
    	
       	Session session = Session.getInstance(props);
       	
       	try {
      		 
   			Message message = new MimeMessage(session);
   			message.setFrom(new InternetAddress("vyeluri@swen90002-21.cis.unimelb.edu.au"));
   			message.setRecipients(Message.RecipientType.TO,
   				InternetAddress.parse(email));
   			message.setSubject("SWEN90002 group chat password information");
   			message.setText(msg);
    
   			Transport.send(message);
    
   			System.out.println("Done");
    
   		} catch (MessagingException e) {
   			System.out.println("Failed");
   			throw new RuntimeException(e);
   		}       	
       }
}
