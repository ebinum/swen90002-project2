package swen90002_21.proj2.services;
import java.util.Properties;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HelperService {
	public static final Pattern validEmail = 
		    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
	
	 /*
     * To generate Random token
     */
    
    public String GenerateToken(String token, int n)
    {
    	char[] chars = token.toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < n; i++) {
		    char c = chars[random.nextInt(chars.length)];
		    sb.append(c);
		}
		String randomStr = sb.toString();
		
		return randomStr;
    }
}
