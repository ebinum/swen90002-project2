/**
 * 
 */
package swen90002_21.proj2.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

import org.lightcouch.CouchDbClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import swen90002_21.proj2.models.GenericResponse;
import swen90002_21.proj2.services.HelperService;
import swen90002_21.proj2.services.Mailer;
import swen90002_21.proj2.services.DbClientFactory;

/**
 * @author BELLARKS
 *
 */
public class BaseController {
	
	@Autowired
	protected CouchDbClient dbClient;
	@Autowired
	protected HelperService helper;
	@Autowired
	protected Mailer mailService;
	
	public static final Pattern validEmail = 
		    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
	
	protected static Map<String,String> SessionInfo = new HashMap<String, String>();
	
//	public static Map<String, String> getSessionInfo() {
//		return SessionInfo;
//	}
//
//	public static void setSessionInfo(Map<String, String> sessionInfo) {
//		SessionInfo = sessionInfo;
//	}
	
	public BaseController() {
		//dbClient = DbClientFactory.getInstance();		
    }
    
	protected Boolean isValidEmail(String email) {
		return validEmail.matcher(email).find();
	}
	
	@ExceptionHandler(Throwable.class)
	public @ResponseBody GenericResponse handleException(Throwable throwable){
		return null;
	 //handle exception
	 }

	public String GenerateToken(String token, int n) {
		char[] chars = token.toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < n; i++) {
		    char c = chars[random.nextInt(chars.length)];
		    sb.append(c);
		}
		String randomStr = sb.toString();
		
		return randomStr;
	}
   
    

}
