package swen90002_21.proj2.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonObject;

import swen90002_21.proj2.dtos.ResponseDto;

/**
 * Servlet implementation class Delete
 */
@Controller
public class DeleteController extends BaseController {


	@RequestMapping(value="/Delete",method=RequestMethod.POST)
	protected @ResponseBody ResponseDto deleteEmail(@RequestParam String email,@RequestParam String access_token,HttpSession session) throws ServletException, IOException {
		// TODO Auto-generated method stub
	    
//	    The Delete Servlet, /Delete takes two parameters, email, and token.
//	    �� The servlet checks the token as for the Logout Servlet and Reset Servlet.
//	    The servlet allows a logged in user to delete their registration.
//	    �� The user will have to register again.
//	    ��
//	    �� The UI should prompt the user to confirm a deletion request before proceeding.
		
//		HttpSession session = request.getSession(true);
//		System.out.println(session.getAttribute("token"));
//		
//		Set mapSet = (Set)SessionInfo.entrySet();
////		
//		Iterator mapIterator = mapSet.iterator();
//        System.out.println("Display the key/value of HashMap.");
//        while (mapIterator.hasNext()) {
//                Map.Entry mapEntry = (Map.Entry)mapIterator.next();
//                // getKey Method of HashMap access a key of map
//                String keyValue = (String) mapEntry.getKey();
//                //getValue method returns corresponding key's value
//                String value = (String) mapEntry.getValue();
//                System.out.println("Key : " + keyValue + " Value : " + value);
//        }
//        
//        String v = "venkatesh@gmail.com";
//		System.out.println("1 Delete -- " + SessionInfo.get(v));
		
		
//		String token = request.getParameter("access_token");
//		String email = request.getParameter("email");
		
		ResponseDto responseObject = new ResponseDto("invalid_email","No account is logged in with this email Id"); 


		List<JsonObject> docs = dbClient.view("_all_docs").query(JsonObject.class);
		
		String EmailId = "";
		int counter = 0;
		String tempRev = "", rev = ""; 
		String token = access_token;
		String accessToken = (String) session.getAttribute(email);
		
		if (accessToken != null)
		{
			if (token.equals(accessToken))
			{
				for (int i=0; i<docs.size(); i++)
				{
					EmailId = docs.get(i).get("id").getAsString();	
					JsonObject son = dbClient.find(JsonObject.class, EmailId);
					tempRev = son.get("_rev").getAsString();
						
					if (email.equals(EmailId))
					{				
						counter++;
						rev = tempRev;
					}
				}
				if (counter >= 1)
				{
					System.out.println("error");
					//Deleting from database and hashmap
					dbClient.remove(email, rev);
					session.removeAttribute(email);
				
					return new ResponseDto("account_deleted","User was deleted successfully"); 
//					responseObject.addProperty("status", "account_deleted");
//					responseObject.addProperty("message", "User was deleted successfully");					
				}
//				else 
//				{
//					responseObject.addProperty("status", "false");
//					responseObject.addProperty("message", "Something went wrong with server, please try again.");
//				}
			}
			else
			{
				return new ResponseDto("invalid_token","The token given was invalid"); 
//				responseObject.addProperty("status", "invalid_token");
//				responseObject.addProperty("message", "The token given was invalid");
			}
		}
//		else
//		{
//			responseObject.addProperty("status", "invalid_email");
//			responseObject.addProperty("message", "No account is logged in with this email Id");			
		//}//
	
		
		//sendResponse(response, gson.toJson(responseObject), "text/json");
		return responseObject;
	}

}
