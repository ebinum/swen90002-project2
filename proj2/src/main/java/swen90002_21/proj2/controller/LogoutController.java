package swen90002_21.proj2.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import swen90002_21.proj2.dtos.ResponseDto;

import com.google.gson.JsonObject;

@Controller
public class LogoutController extends BaseController {

	@RequestMapping(value="/Logout",method=RequestMethod.POST)
	protected @ResponseBody ResponseDto doLogout(@RequestParam String access_token,@RequestParam String email,HttpSession session) throws ServletException, IOException {

		
//		session = request.getSession(true);
//		
//		String email = request.getParameter("email");
		String token = access_token;//request.getParameter("access_token");
		System.out.println("client token "+token);
		ResponseDto responseObject = new ResponseDto("invalid_email","No account is logged in with the give email"); 

//		String v = "vyeluri5@gmail.com";
		String accessToken = (String) session.getAttribute(email);
		System.out.println(accessToken);
		
		if (accessToken != null)
		{
			if (token.equals(accessToken))
			{
				session.removeAttribute("token");
				//SessionInfo.remove(email);
				return  new ResponseDto("logged_out","Logged out successfully!"); 
//				responseObject.addProperty("status", "logged_out");
//				responseObject.addProperty("message", "Logged out successfully!");			
			}
			else 
			{
				return  new ResponseDto("invalid_token","the token given was invalid"); 
//				responseObject.addProperty("status", "invalid_token");
//				responseObject.addProperty("message", "the token given was invalid");		
			}
		}
//		else
//		{
//			responseObject.addProperty("status", "invalid_email");
//			responseObject.addProperty("message", "No account is logged in with the give email");		
//		}
		//sendResponse(response, gson.toJson(responseObject), "text/json");
		System.out.println("logged out!");
		return responseObject;
	}

}
