/**
 * 
 */
package swen90002_21.proj2.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import swen90002_21.proj2.dtos.GroupsDto;
import swen90002_21.proj2.dtos.GroupsResponseDto;
import swen90002_21.proj2.dtos.ResponseDto;
import swen90002_21.proj2.services.GroupService;
import swen90002_21.proj2.services.Mailer;

/**
 * @author BELLARKS
 *
 */
@Controller
@RequestMapping("Groups")
public class GroupsController extends BaseController {
	
	@Autowired
	private GroupService groupService;
	
	@Autowired
	Mailer mailer;
	
	@RequestMapping(method=RequestMethod.POST)
	protected @ResponseBody String createGroup(@RequestParam String username,
			@RequestParam String token,@RequestParam String name,HttpSession session) throws ServletException, IOException {
		
		System.out.println("username: " + username + " token: "+ token + " name: "+ name);
		
		String getAccessToken = SessionInfo.get(username);
		JsonObject response = new JsonObject();
		
		List<JsonObject> user = dbClient.view("user/allusers").key(username)
				.query(JsonObject.class);
		
		System.out.println("users size " + user.size());
		
		if (!user.isEmpty()) {		
		
			String status = groupService.createGroup(name, username);
			
			if (status.equals("created")) {			
				System.out.println("1");
				response.addProperty("status" ,"group_created");
				response.addProperty("message" ,"group has been created");
			}
			else {
				response.addProperty("status" ,"group_exists");
				response.addProperty("message" ,"user already has a group with that name");
			}			
		}
		else {
			response.addProperty("status" ,"invalid_email");
			response.addProperty("message" ,"the email entered was invalid");			
		}
			
		return response.toString();
		
//		return new ResponseDto("group_created", String.format("group %s created successfully",name));
	}
	
	@RequestMapping(method=RequestMethod.GET)
	protected @ResponseBody String getGroups(@RequestParam String username,
			@RequestParam String token,HttpSession session) throws ServletException, IOException {	
		
		System.out.println("username: " + username + " token: "+ token);
		
		List<JsonObject> gn = groupService.getAllGroups(username);
		List<JsonObject> jsonReturn = new ArrayList<JsonObject>();
		
		JsonObject resp = new JsonObject();
		
		resp.addProperty("status", "success");
		resp.addProperty("message", "groups");
		JsonArray groups = new JsonArray();
		
		for (int i=0; i<gn.size(); i++)
		{			
			JsonObject jsonStr = (JsonObject) gn.get(i).get("value");
			groups.add(jsonStr);
		 }
		
		resp.add("groups", groups);;
		//jsonReturn.add(resp);
		
		return resp.toString() ;
		
//		return new GroupsResponseDto("success", "",groups);
	}
	
	@RequestMapping(value = "{group_name}/invite/{invited_user}", method = RequestMethod.POST)
	protected @ResponseBody String invite(@PathVariable String group_name, @PathVariable String invited_user, 
			@RequestParam String username, @RequestParam String token) {		
		System.out.println("invite was hit");
		
		String getAccessToken = SessionInfo.get(username);
		JsonObject response = new JsonObject();
		
		if (getAccessToken.equals(token)) {
			
			List<JsonObject> user = dbClient.view("groups/allgroups").key(username)
					.query(JsonObject.class);
			
			
			if (!user.isEmpty()) {
				System.out.println("1");
				String status = groupService.inviteToGroup(group_name, username, invited_user);
				System.out.println("status:  "+ status);
				
				if (status.equals("Added")) {			
					System.out.println("1");
					response.addProperty("status" ,"success");
					response.addProperty("message" ,"the request was successful");
					
				}
				else {
					response.addProperty("status" ,"error");
					response.addProperty("message" ,"No such user exits");
				}
				
			}
			else {
				response.addProperty("status" ,"invalid_groupname");
				response.addProperty("message" ,"No group exits");	
			}			
		}	
		
		return response.toString();
	}
	
	@RequestMapping(value = "{group_name}/inviteToAccList/{invited_user}", method = RequestMethod.GET)
	protected @ResponseBody String inviteToAcceptList(@PathVariable String group_name, @PathVariable String invited_user, 
			@RequestParam String username) {		
		
		System.out.println("Group name : "+ group_name + " Invited_user :" + invited_user + " Owner : " +username);
		
		String status = groupService.inviteToAccList(group_name, username, invited_user);
		
		mailer.SendMail("Mail is working", "vyeluri5@gmail.com");
		
		return (status != null)  ? "Added" : "Falt";
	}
	
	
	
	@RequestMapping(value = "{group_name}", method = RequestMethod.DELETE)
	protected @ResponseBody String delete(@PathVariable String group_name, @RequestParam String username, 
			@RequestParam String token) {
		
		System.out.println(group_name);
		String getAccessToken = SessionInfo.get(username);
		JsonObject response = new JsonObject();
		
		if (getAccessToken.equals(token)) {
			
			String status = groupService.deleteGroup(group_name, username);
			
			if (status.equals("Deleted")) {
				response.addProperty("status" ,"success");
				response.addProperty("message" ,"the request was successful");
			}
			else {
				response.addProperty("status" ,"error");
				response.addProperty("message" ,"");
			}
		}
		else
		{
			response.addProperty("status" ,"invalid_email");
			response.addProperty("message" ,"the email entered was invalid");
		}	
		
		return response.toString();
	}
	
	@RequestMapping(value = "{group_name}/owner/{owner}/leave", method = RequestMethod.POST)
	protected @ResponseBody String leave(@PathVariable String group_name, @PathVariable String owner, 
			@RequestParam String username, 
			@RequestParam String token) {
		
		String getAccessToken = SessionInfo.get(username);		
		
		JsonObject response = new JsonObject();
		
		if (getAccessToken.equals(token)) {
			
			String status = groupService.deleteFromGroup(group_name, owner, username);
			
			if (status.equals("Removed")) {
				response.addProperty("status" ,"success");
				response.addProperty("message" ,"the request was successful");
			}
			else {
				response.addProperty("status" ,"error");
				response.addProperty("message" ,"an error occurred while trying to delete the group");
			}
			
			System.out.println(status);
		}
		else 
		{
			response.addProperty("status" ,"invalid_email");
			response.addProperty("message" ,"the email entered was invalid");
		}
		
		return response.toString();
	}
	
	@RequestMapping(value = "{group_name}/remove/{user_name}", method = RequestMethod.POST)
	protected @ResponseBody String remove(@PathVariable String group_name, @PathVariable String user_name, 
			@RequestParam String username, 
			@RequestParam String token) {
		
		System.out.println("GName : " +group_name + " member : " +user_name + " owner : " + username );
		
		String getAccessToken = SessionInfo.get(username);		
		
		JsonObject response = new JsonObject();
		
		if (getAccessToken.equals(token)) {
			
			String status = groupService.deleteFromGroup(group_name, username, user_name);
			
			if (status.equals("Removed")) {
				response.addProperty("status" ,"success");
				response.addProperty("message" ,"the request was successful");
			}
			else {
				response.addProperty("status" ,"error");
				response.addProperty("message" ,"an error occurred while trying to delete the group");
			}
			
			System.out.println(status);
		}
		else
		{
			response.addProperty("status" ,"invalid_email");
			response.addProperty("message" ,"the email entered was invalid");
		}
		
		return response.toString();	
	}
	
	/*
	 * Check this one 
	 * test it
	 * 
	 * Add username variable
	 * 
	 */
	@RequestMapping(value = "{group_name}/owner/{owner}/messages", method = RequestMethod.GET)
	protected @ResponseBody String getMessages(@PathVariable String group_name, @PathVariable String owner, 
			@RequestParam String username, 
			@RequestParam String token) {
		
		
		System.out.println("Get ALL msgs controller-----------------------");
		List<JsonObject> status = groupService.getGroupMessage(group_name, username, owner);
		
		String getAccessToken = SessionInfo.get(username);		
		
		JsonObject response = new JsonObject();
		JsonArray groups = new JsonArray();
		
		if (getAccessToken.equals(token)) {
			
			for (JsonObject s : status)
			{		
				JsonObject value = s;
				groups.add(value);			
			}
			
//			System.out.println("Priniting groups " + groups);
			
			if (!status.isEmpty()) {
				response.addProperty("status", "success");
				response.addProperty("message" ,"the request was successful");
				response.add("chat_messages",  groups);		
			}
			else
			{
				response.addProperty("status", "error");
				response.addProperty("message" ,"an error occurred while trying get group messages");
				response.add("chat_messages",  groups);		
			}	
		}			
		return response.toString();
	}
	
	@RequestMapping(value = "{group_name}/owner/{owner}/message/{message}", method = RequestMethod.POST)
	protected @ResponseBody String sendMessage(@PathVariable String group_name, @PathVariable String owner,
			@PathVariable String message, @RequestParam String username, @RequestParam String token) {
		
		System.out.println("Send message controller --GName :" + group_name + "  Owner : " + owner + " message : " + message + " email : " + username + " token : " + token   );
		
		String getAccessToken = SessionInfo.get(username);		
		
		JsonObject response = new JsonObject();
		
		if (getAccessToken.equals(token)) {
			
			String status = groupService.postChatToGroup(group_name, message, username, owner);
			
			if (status.equals("Saved")) {
				response.addProperty("status", "success");
				response.addProperty("message" ,"the request was successful");				
			}
			else {
				response.addProperty("status", "error");
				response.addProperty("message" ,"an error occurred while trying get group messages");
			}			
		}
		else
		{
			response.addProperty("status" ,"invalid_email");
			response.addProperty("message" ,"the email entered was invalid");
		}
	
		return response.toString();
	}
	
	
	
		
}
	

