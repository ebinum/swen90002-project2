/**
 * 
 */
package swen90002_21.proj2.controller;
import swen90002_21.proj2.models.ChatMessage;
import swen90002_21.proj2.services.GroupService;
import swen90002_21.proj2.services.GroupServiceImpl;
import swen90002_21.proj2.dtos.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.lightcouch.CouchDbClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * @author mebinum
 * Responsible for chat Messages
 */
@Controller
public class ChatController extends BaseController {
	@Autowired
	private GroupService groupService;
	
	@RequestMapping(value="Chat",method=RequestMethod.GET)
	public @ResponseBody String getChat(HttpServletResponse response, @RequestParam String username, String group, String owner) throws IOException{
		
		List<ChatMessage> chats = new ArrayList<ChatMessage>();
		
		chats.add(new ChatMessage("hello","james"));
		chats.add(new ChatMessage("hello again","james"));
		chats.add(new ChatMessage("hi ben how goes it","ben"));
		chats.add(new ChatMessage("I am great","james"));
		//dbClient = new CouchDbClient();
		
		//JsonObject Jobject = new JsonObject();
		
//		List<JsonObject> docs = dbClient.view("_all_docs").key("vy").query(JsonObject.class);
//		if (!docs.isEmpty())
//		{
			//GroupService gs = new GroupServiceImpl();
//			gs.postChatToGroup(groupName, chatMessage, username);
			//System.out.println(msg +"  " + group);
//			groupService.postChatToGroup("m", msg, "Mike", owner);
		List<JsonObject> j = groupService.getGroupMessage(group, username, owner);
		
		List<JsonObject> jsonReturn = new ArrayList<JsonObject>();
		
		System.out.println("from controller" + j);
		
		for (int i=0; i<j.size(); i++)
		{
			JsonObject resp = new JsonObject();
			
			JsonObject jsonStr = (JsonObject) j.get(i).get("value");
			
			resp.addProperty("status", "True");
			resp.addProperty("message", "success");
			resp.add("groupMessage", jsonStr);
			
			
			System.out.println("main response "+resp);
			jsonReturn.add(resp);
			
			//JsonObject key = jsonStr.getAsJsonObject("value");
			
			System.out.println("Printing from key "+ jsonStr);
			
		}
		
		if (!jsonReturn.isEmpty())
		{
			return jsonReturn.toString() ;
		}
		else
			return "null";
			
//		[{"message":"hello","user":"james","timestamp":1382578840538},{"message":"hello again","user":"james","timestamp":1382578840538},{"message":"hi ben how goes it","user":"ben","timestamp":1382578840538},{"message":"I am great","user":"james","timestamp":1382578840538}]
//			System.out.println(msg +"  " + group);
//			Test t;
//			JsonObject o = new JsonObject();
//			o.add("object", new Test("abc"));
//			
//			JsonParser parser = new JsonParser();
//			for (int i=0; i<docs.size(); i++)
//			{
//			String id = docs.get(0).get("id").getAsString();
//			
//			//User user = dbClient.find(User.class, id);
//			
//			System.out.println(id);
//			
//			//System.out.println("Is registered " + user.getRegd());
//			//user.getGroup().addMemeber("venkatesh");
//			
//			//System.out.println("Groups " + user.getGroupAsString());
//			
//			
//			JsonObject son = dbClient.find(JsonObject.class, id);
////			System.out.println(son);
//			String tempRev = son.get("_rev").getAsString();
//			System.out.println(tempRev);
//			System.out.println("------------------------------");
//			String owner = son.get("owner").getAsString();
//			System.out.println(owner);
////			System.out.println("Im testing ------ " +owner);
//			//JsonElement gName = son.getAsJsonObject("group").get("groupName");
//			String groupName = son.get("groupName").getAsString();
//			System.out.println(groupName);
//			
//			
//			if (groupName.equals("m"))
//			{
//			
//			JsonArray invitedData = son.getAsJsonObject("object").getAsJsonArray("test");
//			JsonArray acceptedData = son.getAsJsonObject("group").getAsJsonArray("acceptedMembers");
//			System.out.println("JsonArray values -- "+ acceptedData);
//			
//			JsonObject obj1 = new JsonObject();
			//obj1.
			
			
			
//			
//			JsonElement test = obj.get("test");
			//son.a
//			
//			
//			
//			JsonArray finalArray = new JsonArray();
//			acceptedData.add(test);
//			
////			JsonArray f = (JsonArray) son.getAsJsonObject("group").getAsJsonArray("acceptedMembers").getAsJsonArray().get(0);
////			f.
////			System.out.println("To delete --- " + f);
//			
//			//gData.
//			//JsonObject members = gData.get("acceptedMembers").getAsJsonObject();
////			for (JsonElement s : acceptedData) {
////				String k = s.toString();				
////				String t = s.getAsString();
////				if (!t.equals("Fiona"))
////				{
////					finalArray.add(s);
////					
//////					JsonObject r = s.getAsJsonObject();
//////					r.remove("Fiona");
////					//System.out.println( gName + " ---- "+owner+ "--------" + s.getAsString());
////				}
////				else
////				{
////					//finalArray.add(s);
////				}				
////			}
//			
////			for (int i=0; i<acceptedData.size(); i++)
////			{
////				//String t = acceptedData
////				JsonObject r = (JsonObject) acceptedData.get(i);
////				r.remove("Fiona");
////			}
//			
//			//test.add(v);
//			//String rev = docs.get(0).get("_rev").getAsString();
//			
//			//JsonObject value = Json.createObjectBuilder().build();
//			
//			//Jobject.addProperty("test", txt);
////			.add()
//////			Jobject.add("group", {groupName : testGroup});
//			
//			
//			//String 
//			System.out.println("started point 1");
////			String json_text1 = "{\"group\": {\"invitedMembers\": "+ invitedData +",\"acceptedMembers\":"+ finalArray +",\"owner\":"+ owner +"}}";
//			String json_text1 = "{\"group\": {\"invitedMembers\": "+ invitedData +",\"acceptedMembers\":"+ acceptedData +"}}";
//
//			
//			//String json_text = "{\"group\": "+ txt +"}";
//			
//			//System.out.println(json_text);
//			JsonObject o = new JsonObject();
//			
//			
//			o = (JsonObject)parser.parse(json_text1);		
//			System.out.println("2");
//			o.addProperty("_id", id);
//			o.addProperty("_rev", tempRev);
//			o.addProperty("owner", owner);
//			o.addProperty("groupName", groupName);
//			o.addProperty("type","group");
//			
//			//System.out.println(tempRev + "this si me " + id);
//			System.out.println("working1");
//			
//			//Jobject.addProperty("group", "{"acceptedMembers":["mike@gmail.com","venkqat@gmail.com"],"owner":"mike@gmail.com","groupName":"Mike's group"}");
//			dbClient.update(o);
//			System.out.println("working2");
//			break;
//			}
//			
//			}
//		}
//		else
//		{
//			System.out.println("emppty");
//			System.out.println(docs.toString());
//		}
//		if (docs != null)
//		{
//			System.out.println("Chat was hit");
//		}
//		String id = null;
//		for (int i=0; i<docs.size(); i++)
//		{
//			id = docs.get(i).get("id").getAsString();
//			System.out.println(id);
//			System.out.println(docs);
//		}
		
		//return "Chat";
	}
	
	public class Test
	{
		private String name;
		
		public Test(String n)
		{
			this.name = n;
		}
		
		public String toString()
		{
			return name;
		}
	}
	
	public class User 
	{
		private String regd;
		public User() {
		}

		private Group group;

		public Group getGroup() {
			return group;
		}
		
		public String getGroupAsString(){
			return group.toString();
		}
		
//		public String addMember(){
//			return group.addMemeber(String m);
//		}

		public void setGroup(Group group) {
			this.group = group;
		}

		public String getRegd() {
			return regd;
		}

		public void setRegd(String regd) {
			this.regd = regd;
		}
	}
	
	public class Group {
		public Group() {
		}

//		public Group(String[] acceptedMembers, String owner, String groupName) {
//			super();
//			this.acceptedMembers = acceptedMembers;
//			this.owner = owner;
//			this.groupName = groupName;
//		}

		private String[] acceptedMembers;
		private String owner;
		private String groupName;
		public String[] getAcceptedMembers() {
			return acceptedMembers;
		}

		public void setAcceptedMembers(String[] acceptedMembers) {
			this.acceptedMembers = acceptedMembers;
		}
		
		public void addMemeber(String member) {
			this.acceptedMembers[2] = member;
		}

		public String getOwner() {
			return owner;
		}

		public void setOwner(String owner) {
			this.owner = owner;
		}

		public String getGroupName() {
			return groupName;
		}

		public void setGroupName(String groupName) {
			this.groupName = groupName;
		}
		
		public String toString(){
			StringBuffer st = new StringBuffer();
			st.append(groupName + " - ");
			st.append(owner + " -- ");
			for(String member : acceptedMembers){
				st.append(" member: " + member);
			}
			return st.toString();
		}
	}
	private void addProperty(String string, String id) {
		// TODO Auto-generated method stub
		
	}
}
