package swen90002_21.proj2.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import swen90002_21.proj2.dtos.LoginResponseDto;
import swen90002_21.proj2.dtos.ResponseDto;

import com.google.gson.JsonObject;

/**
 * Servlet implementation class Login
 */
@Controller
public class LoginController extends BaseController {


	@RequestMapping(value="/Login",method=RequestMethod.POST)
	protected @ResponseBody String doLogin(@RequestParam String email,@RequestParam String password,HttpSession session) throws ServletException, IOException {
		
//		String email = request.getParameter("email");
//		String password = request.getParameter("password");
//		
		System.out.println(email+" "+ password);
				
		//session = request.getSession(true);
		
		String EmailId;
		int counter = 0;   //Counter to check - email and pswd are matched
		
//		ResponseDto responseObject = new ResponseDto("invalid_email","Invalid email format"); 
		JsonObject responseObject = new JsonObject();
		
		responseObject.addProperty("status", "invalid_email");
		responseObject.addProperty("message", "Invalid email format");	
		
		List<JsonObject> docs = dbClient.view("user/allusers").query(JsonObject.class);
		
		//Validating email format (vyeluri@gmail.com)
		//matcher = validEmail.matcher(email);
		if (isValidEmail(email))
		{		
			/*
			 * If success looping through all docs
			 * to get email and password
			 */
			for (int i=0; i<docs.size(); i++)
			{
				EmailId = docs.get(i).get("id").getAsString();	
				System.out.println(EmailId);
				JsonObject son = dbClient.find(JsonObject.class, EmailId);
				String pswd = son.get("password").getAsString();
				
				System.out.println(EmailId +" - "+ pswd);
			
				if (EmailId.equals(email) && pswd.equals(password))
				{				
					counter++;
					System.out.println("login success 1");
				}
			}
		
			/*
			 * When counter > 1, create a new sessionToken 
			 * with token as an Attribute 
			 * 
			 * Adding email and sessionToken values in Hashmap
			 * for further reference
			 */
			if (counter >= 1)
			{
				String sessionToken = GenerateToken("hur84jbnfc7m603h3kjdi4jgh5f",9);
				session.setAttribute("token", sessionToken);
			
				session.setAttribute(email,sessionToken);
				
				//Adding token in Hashmap 
				SessionInfo.put(email,sessionToken);	
				
				responseObject.addProperty("status", "logged_in");
				responseObject.addProperty("message", "User has been logged in");
				System.out.println("login success");
				//return new LoginResponseDto("logged_in","User has been logged in",sessionToken,email); 
				responseObject.addProperty("access_token", sessionToken);
				responseObject.addProperty("email", email);
				
//				return responseObject;
			}
			else
			{
//				return responseObject = new ResponseDto("login_error","password dont match/Invalid login"); 
				responseObject.addProperty("status", "login_error");
				responseObject.addProperty("message", "password dont match/Invalid login");	
			}	
		}
		else
		{
			responseObject.addProperty("status", "invalid_email");
			responseObject.addProperty("message", "Invalid email format");	
		}
		
		return responseObject.toString();
//		return responseObject;
		
		/*
		 * This code is to get AccessToken stored in HashMap
		 * to test other Servlets 
		 */
		
		//HttpSession session = request.getSession(true);
//		System.out.println(session.getAttribute("token"));
//		
//		//Set mapSet = (Set)SessionInfo.entrySet();
////		
//		Iterator mapIterator = mapSet.iterator();
//        System.out.println("Display the key/value of HashMap.");
//        while (mapIterator.hasNext()) {
//                Map.Entry mapEntry = (Map.Entry)mapIterator.next();
//                // getKey Method of HashMap access a key of map
//                String keyValue = (String) mapEntry.getKey();
//                //getValue method returns corresponding key's value
//                String value = (String) mapEntry.getValue();
//                System.out.println("Key : " + keyValue + " Value : " + value);
//        }
//        
//        String v = "venkatesh@gmail.com";
//		System.out.println("1 Delete -- " + SessionInfo.get(v));
	}

}
