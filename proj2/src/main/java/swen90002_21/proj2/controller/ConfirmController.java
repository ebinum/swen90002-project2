package swen90002_21.proj2.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import swen90002_21.proj2.dtos.ResponseDto;

import com.google.gson.JsonObject;

/**
 * Servlet implementation class Confirm
 */
@Controller
public class ConfirmController extends BaseController {
       
	@RequestMapping(value="/Confirm",method=RequestMethod.POST)
	protected @ResponseBody ResponseDto confirmEmail(@RequestParam String email,@RequestParam String token) throws ServletException, IOException {
//	    String token = request.getParameter("token");
//        String email = request.getParameter("email");
//        
//        
		System.out.println(email + token);
		
        JsonObject Jobject = new JsonObject();
        //String token = access_token;
        List<JsonObject> docs = dbClient.view("user/allusers").key(email).query(JsonObject.class);
       // JsonObject responseObject = new JsonObject(); 
        ResponseDto responseObject = new ResponseDto("invalid_link","Invalid confirmation link"); 

        
        String EmailId;
        int counter = 0;
        
//        for (int i=0; i<docs.size(); i++)
//        {
//            EmailId = docs.get(i).get("id").getAsString();  
//            JsonObject son = dbClient.find(JsonObject.class, EmailId);
//            String tokenField = son.get("token").getAsString();
//                        
//            if (email.equals(EmailId) && token.equals(tokenField))
//            {               
//                counter++;
//                
//            }
//        }
        
        if (!docs.isEmpty())
        {
        	EmailId = docs.get(0).get("id").getAsString();  
        	JsonObject son = dbClient.find(JsonObject.class, EmailId);
        	String tokenField = son.get("token").getAsString();
        	
        	System.out.println(tokenField);
        	
        	if (email.equals(EmailId) && token.equals(tokenField))
              {               
                  counter++;                  
              }
        
        
        
        
        
        
        int insCounter = 0;
        String tempRev = "", rev = "";
        
        if (counter >= 1)
        {           
            System.out.println("Confirmed, redirecting...");
            
            /*
             * To generate random string for password 
             * and send email
             */
            String password = GenerateToken("okjwp954nf9hdqmloidg325iuggr7wixz8", 9);
            System.out.println(password);
            //response.sendRedirect("/registerApp/Login?emailAddress="+ email +"");
            /*
             * looping through all docs to get _rev id of 
             * given email
             */
            
            
            for (int i=0; i<docs.size(); i++)
            {
                EmailId = docs.get(i).get("id").getAsString();  
                JsonObject Json = dbClient.find(JsonObject.class, EmailId);
                tempRev = Json.get("_rev").getAsString();
                System.out.println(rev);
                
                if (email.equals(EmailId))
                {
                    insCounter++;
                    rev = tempRev;
                }
            }
            
            if (insCounter >= 1)
            {
                /*
                 * Updating document with below fields
                 */
                Jobject.addProperty("_id",email);
                Jobject.addProperty("_rev",rev);
                Jobject.addProperty("password",password);
                Jobject.addProperty("regd","true");
                Jobject.addProperty("token",token);
                Jobject.addProperty("type", "user");
                dbClient.update(Jobject);   
                
                
                /*
                 * Calling SendMail method to send email
                 * with username and password
                 */
              mailService.Confirm("Hi,\n\n"
              + "You have been confirmed by us. Please use below username and password to login\n\n"
              + "username: "+ email +"\n"
              + "password: "+ password +"\n\n"
              + "Click the link to login\n"
              + "http://swen90002-21.cis.unimelb.edu.au:8080/proj2/#/login\n\n"
              + "\nCheers\n"
              + "Group - 5", email);     
                
              responseObject = new ResponseDto("email_confirmed","user's email has been confirmed"); 
//                responseObject.addProperty("status", "email_confirmed");
//                responseObject.addProperty("message", "user's email has been confirmed");
            }
        }
            else
            {
            	responseObject = new ResponseDto("confirm_error","An error occurred when attempting to confirm the email"); 
//                responseObject.addProperty("status", "confirm_error");
//                responseObject.addProperty("message", "An error occurred when attempting to confirm the email");
            }
        }
//        else
//        {
//            responseObject.addProperty("status", "invalid_link");
//            responseObject.addProperty("message", "Invalid confirmation link");
//            System.out.println("Fake ID and token");
//        }       
        
        //sendResponse(response, gson.toJson(responseObject), "text/json");
        
        return responseObject;
    }
	

}
