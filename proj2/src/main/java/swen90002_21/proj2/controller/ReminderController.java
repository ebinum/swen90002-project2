package swen90002_21.proj2.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonObject;

import swen90002_21.proj2.dtos.ResponseDto;

/**
 * Servlet implementation class Reminder
 */
@Controller
public class ReminderController extends BaseController {


	@RequestMapping(value="/Reminder",method=RequestMethod.POST)
	protected @ResponseBody ResponseDto sendReminder(@RequestParam String email) throws IOException {
		// TODO Auto-generated method stub
//	    The Reminder Servlet, /Reminder takes a single parameter, email. It sends a Reminder Email to the
//	    email address.
//	    The servlet should respond appropriately to the following conditions:
//	    the email address is invalid, meaning that it is not a proper email address or that the application does
//	    not allow email addresses of that kind
//	    ��
//	    �� the email address is not registered
//	    sending of the reminder email resulted in an error from the email system and the user should try the
//	    reminder again
//	    ��
//	    �� a limit of one reminder email per 5 minutes should be enforced
		
		String EmailId = "";
		String getPassword;
		String finalPswd = "";
		int counter = 0;
		String rev = "";
		String token = "";
		
		ResponseDto responseObject = new ResponseDto("invalid_email","if the email that was sent was invalid"); 
		
//		responseObject.addProperty("status", "invalid_email");
//		responseObject.addProperty("message", "if the email that was sent was invalid");
//		
		if (isValidEmail(email))
		{
			List<JsonObject> docs = dbClient.view("user/allusers").query(JsonObject.class);
			
			for (int i=0; i<docs.size(); i++)
			{
				EmailId = docs.get(i).get("id").getAsString();	
				JsonObject son = dbClient.find(JsonObject.class, EmailId);
				getPassword = son.get("password").getAsString();
				rev = son.get("_rev").getAsString();
				token = son.get("token").getAsString();
				
				if (EmailId.equals(email))
				{
					finalPswd = getPassword;
					counter++;
					break;
				}
				
			}
			
			if (counter >= 1 && finalPswd.equals(""))
			{
				 responseObject = new ResponseDto("reminder_sent","confirmation sent"); 
				 
				 //Generating password
				 String password = GenerateToken("okjwp954nf9hdqmloidg325iuggr7wixz8", 9);
				 
				 JsonObject Jobject = new JsonObject();
				 Jobject.addProperty("_id",email);
	                Jobject.addProperty("_rev",rev);
	                Jobject.addProperty("password",password);
	                Jobject.addProperty("regd","true");
	                Jobject.addProperty("token",token);
	                Jobject.addProperty("type", "user");
	                dbClient.update(Jobject);   
				 
				 System.out.println("Printing pswd " + password);
				 mailService.SendMail(EmailId, password);
				
//				responseObject.addProperty("status", "reminder_sent");
//				responseObject.addProperty("message", "conf sent");
			}
			
			else if (counter >= 1 && !finalPswd.equals(""))
			{
				responseObject = new ResponseDto("reminder_sent","an email was sent out successfully"); 
				mailService.SendMail(EmailId, finalPswd);
				
				
//				responseObject.addProperty("status", "reminder_sent");
//				responseObject.addProperty("message", "an email was sent out successfully");
			}			
			else
			{
				responseObject = new ResponseDto("not_registered","that email address is not registered"); 
//				responseObject.addProperty("status", "not_registered");
//				responseObject.addProperty("message", "that email address is not registered");
			}
			
		}
//		else
//		{
////			responseObject.addProperty("status", "invalid_email");
////			responseObject.addProperty("message", "if the email that was sent was invalid");
//		}	
	
		//sendResponse(response, gson.toJson(responseObject), "text/json");
		return responseObject;
	}

}
