package swen90002_21.proj2.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonObject;

import swen90002_21.proj2.dtos.ResponseDto;

/**
 * Servlet implementation class Reset
 */
@Controller
public class ResetController extends BaseController {

	@RequestMapping(value="/Reset",method=RequestMethod.POST)
	protected @ResponseBody ResponseDto resestPassword(@RequestParam String email,@RequestParam String access_token,HttpSession session) throws IOException {
		//session = request.getSession(true);

		//		String email = request.getParameter("email");
	   String token = access_token;//request.getParameter("access_token");
		//		
		//		String v = "vyeluri5@gmail.com";
		String accessToken = (String) session.getAttribute(email);

		ResponseDto responseObject = new ResponseDto("invalid_email","No account is logged in with the give email");

		JsonObject Jobject = new JsonObject();

		List<JsonObject> docs = dbClient.view("user/allusers").query(JsonObject.class);
		int counter = 0;
		String EmailId;
		String rev = "";
		String dbToken = "";
		if (accessToken != null)
		{
			if (token.equals(accessToken))
			{
				String password = GenerateToken("hur84jbnfc7m603h3kjdi4jgh5f",9);

				for (int i=0; i<docs.size(); i++)
				{
					EmailId = docs.get(i).get("id").getAsString();	
					JsonObject son = dbClient.find(JsonObject.class, EmailId);
					String tempRev = son.get("_rev").getAsString();
					dbToken = son.get("token").getAsString();

					if (email.equals(EmailId))
					{
						counter++;
						rev = tempRev;
					}
				}

				if (counter >= 1)
				{
					Jobject.addProperty("_id",email);
					Jobject.addProperty("_rev",rev);
					Jobject.addProperty("password",password);
					Jobject.addProperty("regd","true");
					Jobject.addProperty("token", dbToken);
					Jobject.addProperty("type", "user");
					dbClient.update(Jobject);	


					/*
					 * Calling SendMail method to send email
					 * with username and password
					 */
									mailService.Reset("Hi,\n\n"
									+ "Your password has been reset. Please use blow username and password to login\n\n"
									+ "username: "+ email +"\n"
									+ "password: "+ password +"\n\n"
									+ "Click the link to login\n"
									+ "http://swen90002-21.cis.unimelb.edu.au:8080/proj2\n\n"
									+ "\nCheers\n"
									+ "Group - 5", email);		
					
					responseObject = new ResponseDto("password_reset","user's password was reset successfully");
//					responseObject.addProperty("status", "password_reset");
//					responseObject.addProperty("message", "user's password was reset successfully");
					System.out.println("Reset done");
				}
				//			
				//			else
				//			{
				//				responseObject.addProperty("status", "false");
				//				responseObject.addProperty("message", "Something went wrong while connecting to DB. Please try again");
				//			}

			}
			else 
			{
				responseObject = new ResponseDto("invalid_token","the token given was invalid");
				//			responseObject.addProperty("status", "invalid_token");
				//			responseObject.addProperty("message", "the token given was invalid");
			}
		}
		//		else
		//		{
		//			responseObject.addProperty("status", "invalid_email");
		//			responseObject.addProperty("message", "No account is logged in with the give email");
		//			System.out.println("invalid email");
		//		}

		//sendResponse(response, gson.toJson(responseObject), "text/json");

		return responseObject;
	}

}
