package swen90002_21.proj2.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import swen90002_21.proj2.dtos.ResponseDto;

import com.google.gson.JsonObject;

/**
 * @author mebinum
 * Responsible for Registration
 */
@Controller
public class RegisterController extends BaseController {
	
	/**
	 * @return 
	 * 
	 */
	@RequestMapping(value="/Register",method=RequestMethod.POST)
	public @ResponseBody ResponseDto registerUser(@RequestParam String email ) throws IOException {
		
		
		//String email = request.getParameter("email");
		ResponseDto responseObject = new ResponseDto("invalid_email","Invalid email format"); 
		String EmailId;
		
		//Email format validation
		if (isValidEmail(email))
		{
			String randomStr = GenerateToken("abc25def4001gh6ijkl58mnopqrstuv974wxyz", 20);	
			
			
			
			List<JsonObject> docs = dbClient.view("user/allusers").query(JsonObject.class);
			int counter = 0;
			
			System.out.println("1---");
			
			/*
			 * To loop through all documents and check email 
			 * exits or not in IF condition and check token {true or false}
			 * 
			 * Token is regd column in Db
			 * 
			 * If {Email,token(false)} => send confirmation link
			 * 	  {Email,token(true)}  => Reset password	
			 */
			
			
			for (int i=0; i<docs.size(); i++)
			{
				
				EmailId = docs.get(i).get("id").getAsString();	
				JsonObject son = dbClient.find(JsonObject.class, EmailId);
				String token = son.get("regd").getAsString();
				
				
							
				if (email.equals(EmailId) && token.equals("false"))
				{
					responseObject = new ResponseDto("is_registered",
							"User exits, Click confirmation link to complete registration.");
//					responseObject.addProperty("status", "is_registered");
//					responseObject.addProperty("message", "User exits, Click confirmation link to complete registration.");
					counter++;
				}								
				else if (email.equals(EmailId) && token.equals("true"))
				{
					responseObject = new ResponseDto("is_registered",
							"User exits, Click forgot password link to get password.");
//					responseObject.addProperty("status", "is_registered");
//					responseObject.addProperty("message", "User exits, Click forgot password link to get password.");
					counter++;
				}
				
				
				
			}
			
			/*
			 * if counter is 0 then process registration 
			 * else return message "Email exits"
			 */
			if (counter == 0)
			{
				System.out.println("Register was hit " + email);
				
				JsonObject Jobject = new JsonObject();
				
				Jobject.addProperty("_id", email);
				Jobject.addProperty("token",randomStr);
				Jobject.addProperty("password","");
				Jobject.addProperty("regd","false");			
				Jobject.addProperty("type","user");	
				
				try {
				dbClient.save(Jobject);			}
				catch (Exception e){
					System.out.println(e);
				}
			
				//Sending back to View
				responseObject = new ResponseDto("new_unverified",
						"User has been created, confirm your email to complete registration.");
//				responseObject.addProperty("status", "new_unverified");
//				responseObject.addProperty("message", "User has been created, confirm your email to complete registration.");
//			
				//Calling SendMail method
				
				System.out.println("1---");
				mailService.Register("Hi,\n\n"
					+ "Your registration is successful. Please click on below link to confirm your email address.\n\n"
					+ "http://swen90002-21.cis.unimelb.edu.au:8080/proj2/#/confirm?token="+ randomStr +"&email="+ email +""
					+ "\nCheers\n"
					+ "Group - 5", email);			
				
				System.out.println("user registered");
			}
//			else
//			{
//				responseObject.addProperty("status", "error");
//				responseObject.addProperty("message", "An error occured while trying to register the user.");
//			}
			
		}
//		else
//		{	
//			
////			responseObject.addProperty("status", "invalid_email");
////			responseObject.addProperty("message", "Invalid email format");
//		}
		
		//sendResponse(response, gson.toJson(responseObject), "text/json");
		return responseObject;		
	}
}
