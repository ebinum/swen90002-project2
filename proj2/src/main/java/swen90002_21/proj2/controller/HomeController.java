package swen90002_21.proj2.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController extends BaseController {

	@RequestMapping(value="/")
	public String index(HttpServletResponse response) throws IOException{
		return "index";
	}
	
	@RequestMapping(value="views/{otherPath}/{filePath}")
	public String getPartials(@PathVariable("otherPath") String otherPath,@PathVariable("filePath") String path) throws IOException{
		System.out.println("other path view requested " + otherPath + " - " + path.toString());
		return otherPath + "/" + path;
	}
	
	@RequestMapping(value="views/{filePath}")
	public String getViews(@PathVariable("filePath") String path) throws IOException{
		System.out.println("view requested was " + path.toString());
		return path;
	}
//	
//	@RequestMapping(value="/{filepath}")
//	public String allPages(@PathVariable("fileRequest") String fileRequest) throws IOException{
//		System.out.println(fileRequest);
//		return fileRequest;
//	}
}
