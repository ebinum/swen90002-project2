'use strict';

var chatApp = angular.module('chatApp', ['ngRoute', 'ngCookies']);

chatApp.config(['$routeProvider', '$httpProvider',
	function($routeProvider, $httpProvider) {
		$routeProvider.when('/register', {
			templateUrl: 'views/register.html',
			controller: 'RegisterCtrl'
		})
			.when('/login', {
				templateUrl: 'views/login.html',
				controller: 'LoginCtrl'
			})
			.when('/confirm', {
				templateUrl: 'views/confirm.html',
				controller: 'ConfirmCtrl'
			})
			// .when('/', {
			// 	templateUrl: 'views/main.html',
			// 	controller: 'MainCtrl'
			// })
			.when('/reminder', {
				templateUrl: 'views/reminder.html',
				controller: 'ReminderCtrl'
			})
			.when('/', {
				templateUrl: 'views/groups.html',
				controller: 'GroupsCtrl'
			})
			.otherwise({
				redirectTo: '/register'
			});
		$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
		//Interceptor for global loading
		$httpProvider.interceptors.push(function($q, $rootScope, stateHandler) {
			var handlerError = function(status, data) {
				var handler = stateHandler[status];
				if (typeof handler === 'undefined') {
					console.error('Undefined handler for status ' + status);
					return;
				}
				// email_confirmed | confirm_error | logged_out | invalid_token | invalid_email
				$rootScope.notify = handler($rootScope, data);
			};
			return {
				'request': function(config) {
					$rootScope.$emit('LoadStarted');
					return config || $q.when(config);
				},
				'requestError': function(rejection) {
					//console.log(rejection); // Contains the data about the error on the request.
					$rootScope.$emit('LoadEnded');
					handlerError(rejection.status, rejection.data);
					// Return the promise rejection.
					return $q.reject(rejection);
				},
				'response': function(response) {
					$rootScope.$emit('LoadEnded');
					return response || $q.when(response);
				},
				'responseError': function(rejection) {
					//console.log(rejection); // Contains the data about the error.
					$rootScope.$emit('LoadEnded');
					// Return the promise rejection.
					handlerError(rejection.status, rejection.data);
					return $q.reject(rejection);
				}
			};
		});

	}
]);

/*
 * Run the application
 * */
chatApp.run(['$rootScope', '$route', '$location', '$cookies', '$http', 'authService','$window',
	function(scope, route, $location, $cookies, $httpProvider, authService,$window) {
		var anonymous = ['/register', '/login', '/confirm', '/reminder'];
		// var any = false;
		// angular.forEach(anonymous, function(value) {
		// 	if ($location.path().toLowerCase() === value) {
		// 		any = true;
		// 	}
		// }, any);
		var redirect = function (route) {
			$location.path(route);
		};
		var isOnAnonymousPage = function() {
			var found = _.find(anonymous,function(path) { return path === $location.path().toLowerCase(); });

			return typeof found !== 'undefined' && found.length > 0;
		};
		//if user is not logged in redirect to the register page
		if (!authService.isLoggedIn() && !isOnAnonymousPage()) {
			console.log('not logged in and tried to access' + $location.path());
			redirect('/register');
		}
		scope.isLoggedIn = function() {
			var loggedIn = authService.isLoggedIn();
			if(!loggedIn && !isOnAnonymousPage()) {
				$window.alert('You are no longer logged into the system, we shall redirect you to the log in page');
				redirect('/login');
			}
			return loggedIn;
		};
		//hide menu when route changes
		scope.$on('$routeChangeStart', function(next, current) {
			//collapse nav bar if is visible
			if ($('.navbar-menu').is(':visible')) {
				$('.navbar-menu').hide();
			}
			//hide modal if visible
			if($('.modal').is(':visible')) {
				$('.modal').modal('hide');
			}
			if($('body').hasClass( 'modal-open' )) {
				$('body').removeClass('modal-open');
			}
		});
	}
]);

//-- Controllers Start

chatApp.controller('HeaderCtrl', ['$scope', '$filter', '$location', 'authService', 'stateHandler',
	function(scope, $filter, $location, authService, stateHandler) {
		var nav = [
		// {
		// 	name: 'home',
		// 	path: '/',
		// 	anonymousAccess: true
		// }, 
			{
				name: 'groups',
				path: '/',
				anonymousAccess: false
			}, {
				name: 'profile',
				path: '/profile',
				anonymousAccess: false
			}];
		var getNav = function() {
			return nav;
			// return _.filter(nav, function(item) {
			// 	return authService.isLoggedIn() || item.anonymousAccess;
			// });
		};
		scope.init = function() {
			scope.navTabs = {};
			scope.navTabs.tabs = getNav();
		};
		// scope.$watch('navTabs.loggedIn', function() {
		// 	scope.navTabs.tabs = _.filter(nav, function(item) {
		// 		return authService.isLoggedIn() || item.anonymousAccess;
		// 	});
		// });
		// global methods available through the scope
		var handle = function(response) {
			var status = response.status === 200 ? $filter('camelcase')(response.data.status) : response.status;

			var handler = stateHandler[status];
			if (typeof handler === 'undefined') {
				console.error('Undefined handler for status ' + status);
				return;
			}
			// email_confirmed | confirm_error | logged_out | invalid_token | invalid_email
			scope.notify = handler(scope, response.data);
		};
		scope.isActive = function(route) {
			return route === $location.path();
		};
		scope.showNavTab = function(name) {
			var tab = _.filter(nav, function(item) {
				return item === name;
			});
			if (name === 'home') {
				console.log('for home ' + JSON.stringify(tab));
				console.log('for home');
			}
			return tab.anonymousAccess || authService.isLoggedIn();
		};
		scope.resetPassword = function() {
			authService.resetPassword().then(handle);
		};
		scope.logOut = function() {
			authService.logOut().then(handle);
		};
		scope.deleteAccount = function() {
			authService.deleteAccount().then(handle);
		};
		scope.$on('LoadStarted', function() {
			scope.loading = true;
		});
		scope.$on('LoadEnded', function() {
			scope.loading = false;
		});
		scope.$on('LoggedIn', function() {
			scope.navTabs.loggedIn = true;
			scope.navTabs.tabs = getNav();
			//scope.$digest();
			console.log('loggin happened');
		}, true);
		scope.$on('LoggedOut', function() {
			scope.navTabs.loggedIn = false;
			scope.navTabs.tabs = getNav();
			//scope.$digest();
			console.log('LoggedOut happened');
			$location.path('/login');
		}, true);
	}
]);
/**
** FooterCtrl
*/
chatApp.controller('FooterCtrl', ['$scope', 'groupsService', 'stateHandler', '$filter','$window','$rootScope',
	function($scope, groupsService, stateHandler, $filter,$window,$rootScope) {
		var handleFail = function  (status,data) {
			var handler = stateHandler[status];
			if (typeof handler === 'undefined') {
				console.log('Undefined handler for status ' + status);
				return;
			}
			$scope.notify = handler($scope, data, 'groups');
		};
		var handleResponse = function(response) {
			var status = response.data ? $filter('camelcase')(response.data.status) : 'noData';
			//if the request was not successful
			if (status !== 'groupCreated') {
				handleFail(status,response.data);
				$window.alert('There was an error creating this group please try again\n'+$scope.notify.message);
				return;
			}
			$rootScope.$broadcast('Groups_Updated');
			$window.alert('Group ' + $scope.groupToCreate + ' created successfully');
			//$scope.$broadcast('Groups_Updated');
		};
		$scope.newGroup = function() {
			var groupName = $window.prompt('Please enter a name for the group ', 'Group name');
			if (groupName) {
				$scope.groupToCreate = groupName;
				groupsService.createGroup(groupName).then(handleResponse);
			}
		};
		$scope.isInvalid = function(control) {
			var form = $scope.sendChatForm;
			return form[control].$dirty && form[control].$invalid || ($scope.empty && form[control].$pristine);
		};
		$scope.sendMessageToGroup = function () {
			if ($scope.sendChatForm.$invalid) {
				$scope.empty = true;
				console.log('send chat invalid');
				return;
			}
			//var selectedGrp = $scope.groups[$scope.selectedGroup];
			groupsService.sendMessageToGroup($scope.chatMessage,$scope.selectedGroup).then(function() {
				$rootScope.$broadcast('Groups_Updated');
				//$scope.$emit('Groups_Updated');
			});
		};
		$scope.getGroups = function() {
			groupsService.getAllGroups().success(function () {
				var data = arguments[0];
				if(data) {
					$scope.groups = data.groups;
				}
			}).error(function() {
				var data = arguments[0];
				var status = $filter('camelcase')(data.status);
				handleFail(status,data);
			});
		};
		if($scope.isLoggedIn()){
			$scope.getGroups();
		}
		$scope.$on('LoggedIn', function() {
			$scope.getGroups();
		}, true);
		$scope.$on('Groups_Updated', function() {
			$scope.getGroups();
		}, true);
	}
]);
//Chat Form Controller
chatApp.controller('ChatFormCtrl', function($scope, Chats) {
	$scope.sendChat = function() {
		if ($scope.isChatValid()) {
			return;
		}
		var msg = {};
		angular.copy($scope.chat, msg);
		Chats.addChat(msg);
		$scope.chat = {
			username: msg.username
		};
	};

	$scope.isChatValid = function() {
		return $scope.chatForm.$invalid;
	};
});



/**
 * Main Controller
 * */
chatApp.controller('MainCtrl', ['$scope', '$cookies', '$http', 'stateHandler', '$filter', 'Chats', 'Poller', '$location', 'authService',
	function($scope, $cookies, $http, stateHandler, $filter, Chats, Poller, $location, authService) {

		if (!authService.isLoggedIn()) {
			console.log('not logged in redirect');
			$location.path('/register');
		}
		//check to see if there is a new chat message added
		//$scope.chatMessages = Chats.getChats();

		//Start the polling
		Poller.start();
		$scope.$on('poll-updated', function(event, data) {
			//console.log('timer data ' + JSON.stringify(data.msgs));
			console.log('poll updated');
			$scope.chatMessages = data.msgs;
			$scope.lastUpdated = data.lastUpdated;
			$scope.calls = data.calls;
		}, true);

		$scope.init = function() {
			$scope.notify = {
				show: false
			};
		};

	}
]);

/**
 * Login Controller
 **/
chatApp.controller('LoginCtrl', ['$scope', '$routeParams', '$http', 'stateHandler', '$filter', '$location',
	function($scope, $routeParams, $http, stateHandler, $filter, $location) {

		if (typeof $routeParams['email'] !== 'undefined') {
			$scope.email = $routeParams['email'];
		}

		$scope.init = function() {
			$scope.notify = {
				show: false
			};
		};

		var handleLogin = function(response) {
			var status = response.status === 200 ? $filter('camelcase')(response.data.status) : response.status;

			//if it's not an error hide the form
			if (status.toLowerCase().indexOf('error') < 0) {
				$scope.hideForm = true;
			}

			var handler = stateHandler[status];
			if (typeof handler === 'undefined') {
				console.error('Undefined handler for status ' + status);
				return;
			}

			$scope.notify = handler($scope, response.data);
		};

		$scope.login = function Login() {
			var loginForm = $scope.loginForm;
			$scope.submitted = true;
			if (loginForm.$invalid) {
				return;
			}

			var postData = {
				method: 'POST',
				url: 'Login',
				params: {
					email: $scope.email,
					password: $scope.password
				}
			};

			$http(postData)
				.then(handleLogin);
		};


	}
]);

/**
 * Register Controller
 **/
chatApp.controller('RegisterCtrl', ['$scope', '$http', '$location', '$filter',
	function($scope, $http, $location, $filter) {
		var setNotification = function(data) {
			$scope.notify = data;
		};
		var hideform = function() {
			$scope.hideForm = true;
		};
		var stateHandler = {};

		stateHandler.newUnverified = function() {
			hideform();
			var notify = {};
			notify.show = true;
			notify.message = 'Thanks for registering with us, an email has been sent to your mailbox.\n' +
				'Please click on the confirmation link in the email to finish your registration';
			notify.nextStep = function() {
				$location.path('/login');
			};
			setNotification(notify);
		};
		//user is already registered successfully
		stateHandler.isRegistered = function() {
			hideform();
			var responseData = arguments[1];
			var notify = {};
			notify.show = true;
			notify.message = 'This email ' + $scope.email + ' has been registered.\n' + responseData.message + '\n Then click continue to login';
			notify.nextStep = function() {
				$location.path('/login').search({
					email: $scope.email
				});
			};

			setNotification(notify);
		};

		stateHandler.invalidEmail = function() {
			var responseData = arguments[1];
			var notify = {};
			notify.show = true;
			notify.message = 'The email you entered was invalid ' + responseData.message;
			notify.nextStep = function() {
				notify.show = false;
			};

			setNotification(notify);
		};

		stateHandler.error = function() {
			var notify = {};
			notify.show = true;
			notify.message = 'An error occurred while trying to process your request\n ' +
				'Please try registering again or contact our support';
			notify.nextStep = function() {
				notify.show = false;
			};

			setNotification(notify);
		};

		var successCallback = function(data, status, headers, config) {
			console.log('returned with ' + status);
		};

		var errorCallback = function(data, status, headers, config) {
			console.log('something happened' + data);
			console.log('status message' + status);
			console.log('headers ' + headers + '\n');
			stateHandler.error();
		};
		//data: new_unverified | is_registered | invalid_email | error

		var handleRegisteration = function(response) {
			var status = $filter('camelcase')(response.data.status);
			var handler = stateHandler[status];
			if (typeof handler === 'undefined') {
				console.log('Undefined handler for status ' + status);
				return;
			}
			handler($scope, response.data);
		};

		$scope.register = function Register() {
			var regForm = $scope.regForm;

			if (regForm.$invalid) {
				console.log('Invalid');
				$scope.empty = true;
				return;
			}

			var postData = {
				method: 'POST',
				url: 'Register',
				params: {
					email: $scope.email
				}
			};

			$http(postData)
				.success(successCallback)
				.error(errorCallback)
				.then(handleRegisteration);
		};
		//$scope.email
	}
]);


/**
 * Confirmation Controller
 * */
chatApp.controller('ConfirmCtrl', ['$scope', '$location', 'stateHandler', '$http', '$filter',
	function($scope, $location, stateHandler, $http, $filter) {
		var email = $location.search()['email'];
		var token = $location.search()['token'];

		$scope.init = function Init() {
			$scope.confirm = {};
		};

		$scope.nextStep = function() {
			$location.path('/login');
		};

		var handleConfirmation = function(response) {
			var status = $filter('camelcase')(response.data.status);
			var handler = stateHandler[status];
			if (typeof handler === 'undefined') {
				console.log('Undefined handler for status ' + status);
				return;
			}
			// email_confirmed | confirm_error | invalid_link
			$scope.confirm = handler($scope, response.data);
		};

		if (typeof email === 'undefined' || typeof token === 'undefined') {
			$scope.confirm.message = 'Apologies we could not confirm\nThe email Address given or token was invalid';
			return;
		}

		var postData = {
			method: 'POST',
			url: 'Confirm',
			params: {
				email: email,
				token: token
			}
		};

		$http(postData)
			.then(handleConfirmation);
	}
]);

/*
 * Reminder Controller
 * */
chatApp.controller('ReminderCtrl', ['$scope', 'stateHandler', '$http', '$filter',
	function($scope, stateHandler, $http, $filter) {

		$scope.init = function() {
			$scope.status = {};
		};

		var handleReminder = function(response) {
			var status = $filter('camelcase')(response.data.status);
			var handler = stateHandler[status];
			if (typeof handler === 'undefined') {
				console.log('Undefined handler for status ' + status);
				return;
			}

			//reminder_sent || not_registered || invalid_email
			$scope.status = handler($scope, response.data);
		};

		$scope.sendReminder = function() {
			if ($scope.reminderForm.$invalid) {
				console.log('Invalid');
				return;
			}

			// reminder_sent || not_registered || invalid_email
			var postData = {
				method: 'POST',
				url: 'Reminder',
				params: {
					email: $scope.email
				}
			};

			$http(postData).then(handleReminder);
		};
	}
]);

/*
 * Groups Controller
 */
chatApp.controller('GroupsCtrl', ['$scope', '$filter', 'groupsService', 'stateHandler','authService','$location','Poller',
	function($scope, $filter, groupsService, stateHandler,authService,$location,Poller) {
		if (!authService.isLoggedIn()) {
			console.log('not logged in redirect');
			$location.path('/register');
		}
		$scope.init = function() {
			//$scope.groups = [];
			$scope.groupName = '';
			$scope.modal = {};
			$scope.modal.invite = {};
			$scope.modal.invite.notify = {};
			$scope.modal.message = {};
			$scope.modal.message.notify = {};
		};
		$scope.modalReset = function(name) {
			$scope.modal[name] = {};
			$scope.modal[name].notify = {};
		};
		var successCallback = function() {
			var data = arguments[0];
			var modal = arguments[4];
			var message = data.message && data.message.length > 0 ? data.message : 'Your request was sent but we did not recieve a response';
			$scope.modal[modal].notify = {
				show:true,
				message: message
			};
			//$scope.$emit('Groups_Updated');
		};
		var errorCallback = function() {
			var data = arguments[0];
			var modal = arguments[4];

			var handler = stateHandler[data.status];
			if (typeof handler === 'undefined') {
				console.log('Undefined handler for status ' + status);
				return;
			}
			$scope.modal[modal].notify = handler($scope, data, 'groups');
		};
		var handleResponse = function(response) {
			var status = response.data ? $filter('camelcase')(response.data.status) : 'noData';
			//if the request was not successful
			if (status !== 'success') {
				var handler = stateHandler[status];
				if (typeof handler === 'undefined') {
					console.log('Undefined handler for status ' + status);
					return;
				}
				console.log('groups error occurred');
				// error || invalid_email
				$scope.notify = handler($scope, response.data, 'groups');
				return;
			}
			//add new property for isOwner and noMembers
			var newlist = _.map(response.data.groups,function (grp) {
				grp.isOwner = grp.owner.toLowerCase() === authService.currentUser().toLowerCase();
				grp.noMembers = (grp.accepted_members.length === 0 && grp.invited_members.length === 0);
				return grp;
			});
			//console.log(JSON.stringify(newlist));
			//$scope.$broadcast('Groups_Updated');
			//$scope.$emit('Groups_Updated');
			$scope.groups = newlist;
		};
		$scope.anyGroups = function() {
			var any = $scope.groups && $scope.groups.length > 0;
			return any;
		};
		$scope.toggleModal = function(name) {
			$(name).modal('toggle', {
				keyboard: false
			});
		};
		$scope.isInvalid = function(formName) {
			var form = $scope[formName];
			return form.$dirty && form.$invalid || ($scope.empty && form.$pristine);
		};
		$scope.resetCreate = function() {
			$('#success-alert').alert('close');
			$('#createGroupModal').modal('hide');
			$scope.notify = {};
		};
		$scope.createGroup = function() {
			if (!$scope.groupName) {
				$scope.empty = true;
				return;
			}
			groupsService.createGroup($scope.groupName).then(handleResponse);
		};
		$scope.getGroups = function() {
			// var dummyGroups = [{
			// 	group_name: 'group name 34',
			// 	owner: 'mebinum@gmail.com',
			// 	invited_members: ['jay@hello.com'],
			// 	accepted_members:['pa@yelp.com']
			// },{
			// 	group_name: 'group 24',
			// 	owner: 'james@gmail.com',
			// 	invited_members: ['jay@hello.com'],
			// 	accepted_members:['pa@yelp.com']
			// }];
			// handleResponse({data:{status:'success',groups:dummyGroups}});
			groupsService.getAllGroups().then(handleResponse);
		};
		$scope.inviteUser = function(index) {
			var selectedGrp = {};
			angular.copy($scope.groups[index], selectedGrp);
			$scope.modal.invite.group = selectedGrp;
			$scope.toggleModal('#inviteUserToGroup');
		};
		$scope.sendMessage = function(index) {
			var selectedGrp = {};
			angular.copy($scope.groups[index], selectedGrp);
			$scope.modal.message.group = selectedGrp;
			$scope.toggleModal('#messageGroup');
		};
		$scope.sendInvite = function() {
			if($scope.isInvalid('inviteUserForm')){
				return;
			}
			groupsService.
			inviteUserToGroup($scope.modal.invite.user,$scope.modal.invite.group.group_name)
			.success(function(data, status, headers, config) {
				successCallback(data, status, headers, config,'invite');
			})
			.error(function (data, status, headers, config) {
				errorCallback(data, status, headers, config,'invite');
			});
		};
		$scope.messageGroup = function() {
			groupsService.
			sendMessageToGroup($scope.modal.message.messageText,$scope.modal.message.group)
			.success(function(data, status, headers, config) {
				successCallback(data, status, headers, config,'message');
			})
			.error(function (data, status, headers, config) {
				errorCallback(data, status, headers, config,'message');
			});
		};
		$scope.deleteGroup = function(index) {
			var selectedGrp = {};
			angular.copy($scope.groups[index], selectedGrp);
			groupsService.deleteGroup(selectedGrp).then(handleResponse);
		};
		$scope.leaveGroup = function(index) {
			var selectedGrp = {};
			angular.copy($scope.groups[index], selectedGrp);
			groupsService.leaveGroup(selectedGrp).then(handleResponse);
		};
		$scope.removeMemberFromGroup = function(groupIndex,memberIndex) {
			//console.log('groupIndex' + groupIndex +' member '+ memberIndex);
			var selectedGrp = {};
			angular.copy($scope.groups[groupIndex], selectedGrp);
			var userToRemove = selectedGrp.accepted_members[memberIndex];
			//console.log('member to remove '+ toRemove);
			groupsService.removeFromGroup(selectedGrp.group_name,userToRemove).then(handleResponse);
		};
		//check if current user is owner of group
		$scope.isOwner = function(index) {
			return $scope.groups[index].owner.toLowerCase() === authService.currentUser().toLowerCase();
		};
		$scope.getGroups();
		$scope.$on('Groups_Updated',function () {
			console.log('Recieved message updaing groups');
			$scope.getGroups();
		},true);
		Poller.start();
		$scope.$on('poll-updated',function () {
			console.log('Poller update groups');
			$scope.getGroups();
		},true);
	}
]);


chatApp.directive('nextSteps', function() {
	return {
		restrict: 'EA',
		replace: true,
		require: 'ngModel',
		template: '<div class="notify alert" data-ng-show="notify.show">' + '<p data-ng-bind="notify.message"></p>' + '<button id="continueButton" class="btn btn-default">Continue</button></div>',
		scope: {
			notify: '=ngModel'
		},
		link: function(scope, elem, attrs) {
			scope.$watch('notify.message', function(newVal, oldVal) {
				console.log(newVal);
				var btn = elem.find('button');
				btn.bind('click', function() {
					scope.$apply(function() {
						//call the controller focus function
						scope.notify.nextStep();
					});
				});
			}, true);
		}
	};
});

chatApp.factory('stateHandler', ['$rootScope', '$location','$cookies',
	function($rootScope, $location,$cookies) {

		//	var messages = {};
		//	messages.delete_request = '';
		//	messages.loggedout_request = 'You have been logged out';
		//	messages. = 'Your security token was invalid and you have been logged out';
		// var deleteCookie = function(name) {
		// 	document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
		// };

		var deleteTokenSetMessage = function(msg) {
			delete $cookies['access_token'];
			delete $cookies['email'];
			//deleteCookie('access_token');
			//deleteCookie('email');
			document.cookie =
				'access_token="";expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
			document.cookie =
				'email="";expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/';
			var notify = {};
			notify.show = true;
			notify.message = msg;
			notify.nextStep = function() {
				$location.path('/register');
			};
			$rootScope.$broadcast('LoggedOut');
			return notify;
		};

		var notifyWithHideNext = function(msg) {
			var outcome = arguments[1];
			var notify = {};
			notify.show = true;
			notify.message = msg;
			notify.nextStep = function() {
				this.show = false;
			};
			if(outcome && outcome === 'error') {
				notify.outcome = 'error';
			}else {
				notify.outcome = 'success';
			}

			return notify;
		};

		var stateHandler = {};

		stateHandler.emailed = function() {
			var notify = {};
			notify.show = true;
			notify.message = 'Thanks for registering with us, an email has been sent to your mailbox.\n\n' +
				'Please click on the confirmation link in the email to finish your registeration';
			notify.nextStep = function() {
				notify.show = false;
			};
			return notify;
		};

		//user is already registered successfully
		stateHandler.registered = function() {
			var scope = arguments[0];
			var notify = {};
			notify.show = true;
			notify.message = 'This email ' + scope.email + ' has already been registered, click continue to login';
			notify.nextStep = function() {
				$location.path('/login').search({
					email: scope.email
				});
			};

			return notify;
		};

		//user was successfully logged in
		stateHandler.loggedIn = function() {
			var responseData = arguments[1];
			var notify = {};
			notify.show = true;
			notify.message = 'You have been successfully logged in';
			notify.nextStep = function() {
				$location.path('/');
			};

			//set expiration to 10 minutes
			var now = new Date();
			var time = now.getTime();
			time += 600 * 1000;
			now.setTime(time);
			document.cookie =
				'access_token=' + responseData.access_token +
				'; expires=' + now.toGMTString() +
				'; path=/';
			document.cookie =
				'email=' + responseData.email +
				'; expires=' + now.toGMTString() +
				'; path=/';
			//let everyone know you are logged in
			$rootScope.$broadcast('LoggedIn');
			//$cookies.access_token = responseData.access_token;
			return notify;
		};

		//there was an error logging the user into the system
		stateHandler.loginError = function() {
			var responseData = arguments[1];
			var notify = {};
			notify.show = true;
			notify.message = 'There was an error logging you onto the system\n ' + responseData.message;
			notify.nextStep = function() {
				this.show = false;
			};

			return notify;
		};

		//confirmation errors - email_confirmed | confirm_error 
		stateHandler.emailConfirmed = function() {
			var responseData = arguments[1];
			var notify = {};
			notify.show = true;
			notify.message = 'Congrats your email has been confirmed\n' +
				'Check your email for a password';
			console.info('server message ' + responseData.message);

			return notify;
		};

		//confirm_error
		stateHandler.confirmError = stateHandler.invalidLink = function() {
			var responseData = arguments[1];
			var notify = {};
			notify.show = true;
			notify.message = 'An error occurred while trying to confirm your email\n' + responseData.message;
			return notify;
		};


		//	reminder_sent
		//	not_registered
		//	reminder_failed
		stateHandler.reminderSent = function() {
			var scope = arguments[0];
			var notify = {};
			notify.show = true;
			notify.message = 'A reminder email has been sent to ' + scope.email + ' with your password, click continue to login';
			notify.nextStep = function() {
				$location.path('/login').search({
					email: scope.email
				});
			};

			return notify;
		};

		stateHandler.notRegistered = function() {
			var scope = arguments[0];
			var notify = {};
			notify.show = true;
			notify.message = 'This email ' + scope.email + ' is not registered, click to Register';
			notify.nextStep = function() {
				$location.path('/register').search({
					email: scope.email
				});
			};

			return notify;
		};

		stateHandler.reminderFailed = function() {
			var notify = {};
			notify.show = true;
			notify.message = 'We could not send out a reminder at this time, please try again later';
			notify.nextStep = function() {
				$location.path('/reminder');
			};

			return notify;
		};

		//account_deleted - the user's account has been deleted successfully
		stateHandler.accountDeleted = function() {
			return deleteTokenSetMessage('Your account was deleted successfully and You have been logged out');
		};

		//Password had been reset
		stateHandler.passwordReset = function() {
			var responseData = arguments[1];
			return notifyWithHideNext('Success \n' + responseData.message);
		};

		//for log out - logged_out
		stateHandler.loggedOut = function() {
			return deleteTokenSetMessage('You have been logged out');
		};


		// invalid_token | invalid_email
		stateHandler.invalidToken = function() {
			return deleteTokenSetMessage('Your security token was invalid and you have been logged out');
		};

		stateHandler.invalidEmail = function() {
			var callType = arguments[0];
			if (!callType) {
				return deleteTokenSetMessage('Your email was invalid and you have been logged out');
			}
			return notifyWithHideNext('An error occured while processing your request for ' + callType,'error');
		};

		stateHandler.groupCreated = function() {
			var responseData = arguments[1];
			var notify = {};
			notify.show = true;
			notify.message = responseData.message;
			return notify;
		};

		stateHandler.groupExists = function() {
			return notifyWithHideNext('Could not create group because you already have a group with that name','error');
		};

		stateHandler.noData = function() {
			return notifyWithHideNext('Your request was successful but our servers did not respond. Can you try again please','error');
		};

		stateHandler.error = function() {
			var callType = arguments[0];
			return notifyWithHideNext('An error occured while processing your request to ' + callType ? callType : 'the server','error');
		};
		//http errors
		stateHandler[500] = function() {
			return notifyWithHideNext('Oops, an error occured while we where processing your request\nPlease try again later','error');
		};

		stateHandler[403] = function() {
			return notifyWithHideNext('You do not have rights to carry out that request','error');
		};

		stateHandler[404] = function() {
			return notifyWithHideNext('Error: The resource you attempted to contact doesn\'t exist on our servers','error');
		};

		stateHandler[400] = stateHandler[501] = function() {
			return notifyWithHideNext('Bad Request - Apologies we could not fulfill your request because of a bad syntax','error');
		};


		return stateHandler;
	}
]);

chatApp.service('Chats', ['$rootScope', '$http',
	function($rootScope, $http) {
		var data = [];

		var successCallback = function(data, status, headers, config) {
			//console.log('chat was sent ' + JSON.stringify(data));
			config.data.sent = true;
			config.data.timestamp = data.timestamp;
		};

		var errorCallback = function(data, status, headers, config) {
			console.log('chat failed to send' + data);
			console.log('status message' + status);
			config.data.sent = false;
		};

		var addToMessages = function(response) {
			//add the chat data to the array of messages
			//console.log(JSON.stringify(response.config.data));
			data.push(response.config.data);
		};

		return {
			addChat: function(chat) {
				//add post code here
				$http.post('Chat', chat)
					.success(successCallback)
					.error(errorCallback)
					.then(addToMessages);
			},
			data: data,
			getChats: function() {
				return $http.get('Chat').success(function(chatData) {
					data = chatData;
				});
			}
		};
	}
]);

//Authentication Service
chatApp.service('authService', ['$http', '$cookies',
	function($http, $cookies) {

		var postEmailAndToken = function(postUrl) {
			var postData = {
				method: 'POST',
				url: postUrl,
				params: {
					email: $cookies.email,
					access_token: $cookies.access_token
				}
			};
			return $http(postData);
		};
		return {
			isLoggedIn: function() {
				var token = $cookies.access_token;
				var email = $cookies.email;
				return typeof token !== 'undefined' && token.length > 0 && typeof email !== 'undefined' && email.length > 0;
			},
			currentUser: function() {
				return $cookies.email;
			},
			deleteAccount: function() {
				return postEmailAndToken('Delete');
			},

			resetPassword: function() {
				return postEmailAndToken('Reset');
			},
			logOut: function() {
				return postEmailAndToken('Logout');
			}
		};
	}
]);

//Group Service 
chatApp.service('groupsService', ['$http', '$cookies',
	function($http, $cookies) {

		return {
			createGroup: function(groupName) {
				var postData = {
					method: 'POST',
					url: 'Groups',
					params: {
						name: groupName,
						token: $cookies.access_token,
						username: $cookies.email
					}
				};

				return $http(postData);
			},
			getAllGroups: function() {
				var getData = {
					method: 'GET',
					url: 'Groups',
					params: {
						token: $cookies.access_token,
						username: $cookies.email
					}
				};

				return $http(getData);
			},
			inviteUserToGroup: function(invited_user, groupName) {
				var postData = {
					method: 'POST',
					url: 'Groups/' + groupName + '/invite/' + invited_user+ '/',
					params: {
						token: $cookies.access_token,
						username: $cookies.email
					}
				};
				return $http(postData);
			},
			getAllGroupMessages: function(group) {
				var groupName = group.group_name;
				var owner = group.owner;
				var postData = {
					method: 'GET',
					url: 'Groups/'+groupName + '/owner/'+ owner +'/messages/',
					params: {
						token: $cookies.access_token,
						username: $cookies.email
					}
				};
				return $http(postData);
			},
			sendMessageToGroup:function(message,group) {
				var groupName = group.group_name;
				var owner = group.owner;
				var postData = {
					method: 'POST',
					url: 'Groups/' + groupName + '/owner/'+ owner + '/message/' + message+'/',
					params: {
						token: $cookies.access_token,
						username: $cookies.email
					}
				};
				return $http(postData);
			},
			leaveGroup: function(group) {
				var groupName = group.group_name;
				var owner = group.owner;
				var postData = {
					method: 'POST',
					url: 'Groups/' + groupName + '/owner/'+ owner + '/leave/',
					params: {
						token: $cookies.access_token,
						username: $cookies.email
					}
				};
				return $http(postData);
			},
			deleteGroup: function(group) {
				var groupName = group.group_name;
				//var owner = group.owner;
				var postData = {
					method: 'DELETE',
					url: 'Groups/' + groupName,
					params: {
						token: $cookies.access_token,
						username: $cookies.email
					}
				};
				return $http(postData);
			},
			removeFromGroup: function(groupName,username) {
				var postData = {
					method: 'POST',
					url: 'Groups/' + groupName + '/remove/' + username+'/',
					params: {
						token: $cookies.access_token,
						username: $cookies.email
					}
				};
				return $http(postData);
			}
		};
	}]);
//Poller Service
chatApp.factory('Poller', ['$timeout', 'Chats', '$rootScope',
	function($timeout, Chats, $rootScope) {
		var data = {
			msgs: [],
			lastUpdated: new Date(),
			calls: 0
		};

		//polling
		var updateTimer = function() {
			console.log('polling here ');
			Chats.getChats().then(function(data) {
				//console.log(data);
				//console.log('chats returned ' + JSON.stringify(data.data));
				data.msgs = data.data;
				data.lastUpdated = new Date();
				data.calls += 1;
				console.log('updateTimer: ' + data.lastUpdated);
				$rootScope.$broadcast('poll-updated', data);
			});
			//poll the server every 10 secs
			$timeout(updateTimer, 10000);
		};
		return {
			data: data,
			start: function() {
				updateTimer();
			}
		};
	}
]);

//-- Services End

//-- Directives Start

//detect when enter is typed
chatApp.directive('onEnter', function() {
	var linkFn = function(scope, element, attrs) {
		element.bind('keypress', function(event) {
			if (event.which === 13) {
				scope.$apply(function() {
					scope.$eval(attrs.onEnter);
				});
				event.preventDefault();
			}
		});
	};
	return {
		link: linkFn
	};
});

/**
* Redraw directive
**/
chatApp.directive('redraw', function(Chats) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			scope.$watch('chatMessages', function() {
				//when chatMessages get updated
				//Scroll to bottom if the height of the body is greater than scrollheight
				if (document.body.clientHeight >= document.body.scrollHeight) {
					window.scrollTo(0, document.body.scrollHeight);
				}
			}, true);
		}
	};
});

/*
* Chat Messages Directive 
**/
chatApp.directive('groupMessages', ['groupsService','$filter','stateHandler','$rootScope',
	function(groupsService,$filter,stateHandler,$rootScope) {

	return {
		restrict: 'EA',
		require: 'ngModel',
		scope: {
			model:'=ngModel'
		},
		replace: true,
		templateUrl:'views/partials/groupMessages.html',
		link: function(scope, element, attrs) {
			var handleResponse = function(response) {
				var status = response.data ? $filter('camelcase')(response.data.status) : 'noData';
				//if the request was not successful
				if (status !== 'success') {
					var handler = stateHandler[status];
					if (typeof handler === 'undefined') {
						console.log('Undefined handler for status ' + status);
						return;
					}
					// error || invalid_email
					scope.outcome = handler(scope, response.data, 'groups');
					return;
				}
				scope.messages = response.data.chat_messages;
			};
			var update = function() {
				var group = {group_name: scope.model.group_name,owner: scope.model.owner};
				// var group = {group_name: attrs.groupname,owner: attrs.owner};
				groupsService.getAllGroupMessages(group)
					.then(handleResponse);
			};
			scope.$watch('model', function() {
				console.log('model directive update ');
				update();
			},true);
			$rootScope.$on('Groups_Updated',function() {
				console.log('event directive update ');
				update();
			},true);
			$rootScope.$on('poll-updated', function() {
				console.log('Poller update chatMessages');
				update();
			}, true);
		}
	};
}]);

//directive for navigation
chatApp.directive('chatNav', ['$location',
	function($location) {

		return {
			restrict: 'E',
			require: 'ngModel',
			scope: {
				model: '=ngModel'
			},
			replace: true,
			template: '<i id="nav"></i>',
			link: function(scope, tElement, tAttrs) {
				//return {
				//post: 
				//function postLink(scope, tElement, tAttrs, controller) {
				//console.log('scope compile' + JSON.stringify(scope.tabs));
				scope.$watch('model.loggedIn', function(oldVal, newVal) {
					scope.isActive = function(route) {
						var isActive = route === $location.path();
						console.log('check is active ' + route + ' active: ' + isActive);
						return isActive;
					};
					var nav = tElement[0]; //.find('#nav');
					var model = scope.model;
					nav.innerHTML = '';
					if (model.tabs) {
						var appendLi = function(el) {
							var li = document.createElement('li');
							var anchor = document.createElement('a');
							var text = document.createTextNode(_.str.capitalize(el.name));
							var path = el.path ? el.path : '/' + el.name;
							anchor.setAttribute('href', '#' + path);
							anchor.appendChild(text);
							li.setAttribute('data-ng-class', _.str.sprintf('{active:isActive("%s")}', path));
							li.appendChild(anchor);
							//$compile(li)(scope);
							//console.log(li);
							nav.appendChild(li);
						};
						//console.log('tabs bind');
						_.each(model.tabs, function(el, index, array) {
							if (model.loggedIn) {
								appendLi(el);
							} else if (el.anonymousAccess) {
								appendLi(el);
								//$compile(li)
							}
						});
						//$compile(tElement[0])(scope);
						console.log(nav);
					}
				}, true);
				//}
				//};
			}
			// ,
			// link: function(scope,element,attrs) {
			// 	scope.isActive = active;
			//           // '<li class="active"><a href="/">Home</a></li>'+
			//           // '<li><a href="#about">About</a></li>' +
			//           // '<li><a href="#contact">Profile</a></li>
			// 	//element.find('navBar');
			// 	//ng-class="{active:isActive('/dashboard')}"
			// }
		};
	}
]);

chatApp.directive('ngConfirmClick', [
	function() {
		return {
			link: function(scope, element, attr) {
				var msg = attr.ngConfirmClick || 'Are you sure?';
				var clickAction = attr.confirmedClick;
				element.bind('click', function(event) {
					if (window.confirm(msg)) {
						scope.$eval(clickAction);
					}
				});
			}
		};
	}
]);
//-- Directives End

//-- Filters Start
chatApp.filter('humaneDate', function() {
	return function(input) {
		return humaneDate(new Date(input));
	};
});

chatApp.filter('showNavTab', ['authService',
	function(authService) {
		return function(input) {
			console.log('input ' + input);
			return authService.isLoggedIn() || input.anonymousAccess;
		};
	}
]);

/*
 * Converts a string to camelcase
 * */
chatApp.filter('camelcase', function() {

	return function(input) {
		// remove all characters that should not be in a variable name
		// as well underscores an numbers from the beginning of the string
		input = input.replace(/([^a-zA-Z0-9_\- ])|^[_0-9]+/g, "").trim().toLowerCase();
		// uppercase letters preceeded by a hyphen or a space
		input = input.replace(/([ -]+)([a-zA-Z0-9])/g, function(a, b, c) {
			return c.toUpperCase();
		});
		// uppercase letters preceeded by an underscore or a space
		input = input.replace(/([ _]+)([a-zA-Z0-9])/g, function(a, b, c) {
			return c.toUpperCase();
		});
		// uppercase letters following numbers
		input = input.replace(/([0-9]+)([a-zA-Z])/g, function(a, b, c) {
			return b + c.toUpperCase();
		});
		return input;
	};
});