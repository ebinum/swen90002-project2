<%@ page language="java" pageEncoding="US-ASCII"%>
<!-- Browser incompatibility warning -->
    <!--[if lt IE 9]>
    <div class="sixteen columns">
      <div class="row-fluid" style="margin-top: 60px; height: 200px;">
          <p><b>Old Browser Warning!</b> This Email Registration System is designed to work on modern browsers. We see you are using IE8 or below. Please switch to either:
            <ol>
              <li><a href="http://www.google.com/chrome">Chrome</a></li>
              <li><a href="http://www.mozilla.org/en-US/firefox/new/">Firefox</a></li>
              <li><a href="http://www.apple.com/au/safari/">Safari</a></li>
              <li><a href="http://www.microsoft.com/AU/Internet-Explorer">Internet Explorer 9 or 10</a></li>
            </ol>
          </p>
      </div>
    </div>
    <![endif]-->