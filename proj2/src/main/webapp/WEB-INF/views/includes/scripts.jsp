<%@ page language="java"
    pageEncoding="ISO-8859-1"%>
<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->


	<script type="text/javascript" src="resources/bower_components/angular/angular.js"></script>
	<script type="text/javascript" src="resources/bower_components/angular-route/angular-route.js"></script>
	<script type="text/javascript" src="resources/bower_components/angular-bootstrap/ui-bootstrap.js"></script>
	<script type="text/javascript" src="resources/bower_components/angular-cookies/angular-cookies.js"></script>
	<script type="text/javascript" src="resources/lib/humane.js"></script>
	<script type="text/javascript" src="resources/js/app.js"></script>