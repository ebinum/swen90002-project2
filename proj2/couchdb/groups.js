/*
 * Groups View
 */

//Js code to display -- View name -  groups/allgroups

function(doc) {
  if (doc.type === "group"){
  	emit(doc.owner, { "group_name": doc.group_name, "owner": doc.owner, "invited_members":doc.group.invitedMembers, "accepted_members":doc.group.acceptedMembers });
}
}

// View name -- groups/othergroups

function(doc) {
	if (doc.type === "group" && doc.group && doc.group.acceptedMembers) {
		var inGroup = false;
		for(var i = 0;  i < doc.group.acceptedMembers.length;i++){
			if(doc.group.acceptedMembers[i] === doc.owner) {
				inGroup = true;
			}
		}
		if(!inGroup) {
			emit(doc.group.acceptedMembers, { "group_name": doc.group_name, "owner": doc.owner, "invited_members":doc.group.invitedMembers, "accepted_members":doc.group.acceptedMembers });
		}

}
}

