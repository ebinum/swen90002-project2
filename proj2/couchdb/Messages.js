/*
 * Messages View
 */

//Js code to display -- View name -  messages/groupMsgs

function(doc) {
  if (doc.type === "message"){
  	emit(doc.owner,{"group_name":doc.group_name, "posted_by":doc.posted_by,"message_text":doc.message_text, "timestamp":doc.timestamp, "owner":doc.owner}) 

}
}