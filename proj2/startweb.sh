#!/bin/bash
#Shell script to run serve
logDir=./logs/
theTime=`date +%d_%m_%Y_%H%M%S`

echo "Attempting to start the Tomcat server"

if [ ! -d ${logDir} ]; then
  mkdir ${logDir}
fi

exec mvn tomcat7:run > ${logDir}/tomcat_logs.${theTime}.log